/*******************************************************************************
           FILE:  USB_Public.h

    DESCRIPTION:  USB-to-PC interface manager. 

         AUTHOR:  David Russell
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  11/18/19
*******************************************************************************/
#ifndef USB_PUBLIC_H
#define USB_PUBLIC_H

#include <stdint.h>
#include <stdbool.h>

//Public defines, enums, typedefs, etc.

typedef enum
{
    USB_STATE_UNCONFIGURED  = 0,
    USB_STATE_RESET         = 1,
    USB_STATE_IDLE          = 2,
    USB_STATE_RECEIVING     = 3,
    USB_STATE_TRANSMITTING  = 4,
} usb_state_t;

typedef enum
{
    USB_RESERVED_EXIT_REASON                = 0,  //Reserved so that zero is not used as a reason code
    USB_INVALID_STATE_EXIT_REASON           = 1,  //Somehow the state machine ended up in an invalid state
    USB_START_RX_EXIT_REASON                = 2,  //Idle to Rx 
    USB_START_TX_EXIT_REASON                = 3,  //Process to Tx
    USB_PROCESSING_FAILED_EXIT_REASON       = 4,  //DEBUG module did not queue anything as a result of the processing
    USB_START_IDLE_EXIT_REASON              = 5,  //Completed processing, or finished reset for the first time
    USB_CONTINUE_PROCESSING_EXIT_REASON     = 6,  //Completed current transmission, go back and check for more
    USB_CONFIGURATION_CHANGED_EXIT_REASON   = 7,  //USB endpoint configuration changed implying host just connected
} usb_exit_reason_t;

//Public Prototypes

void USB_Start(void);
void USB_Service(void);

void USB_EnterBootloadModeWhenReady(void);
bool USB_Configured(void);

#endif //USB_PUBLIC_H
