/*******************************************************************************
           FILE:  main.c

    DESCRIPTION:  Main program loop and master state machine. This is where
                  the magic happens!

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  11/1/19
*******************************************************************************/
#include <stdint.h>

#include "MASTER_STATE_Public.h"

#include "debug.h"
#include "main.h"
#include "project.h"

/*******************************************************************************
    FUNCTION: main

    SUMMARY:
    Main program loop.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
int main(void)
{
    InitStackTest();
    DEBUG_UART_Start();
    MASTER_STATE_Start();

    while(1)
    {
        MASTER_STATE_Service();
    }

    return 0;
}

/*******************************************************************************
    FUNCTION: InitStackTest

    SUMMARY:
    Load the top of the stack with the test pattern. Stack overflow can then be
    checked by halting the program and looking at the address at "PSOC_STACK_END"
    If PSOC_STACK_END contains data other than the TEST_PATTERN you know that the
    stack has overflowed. 

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void InitStackTest(void)
{
    // Pointer to the last word in the stack
    uint16_t *stack = (uint16_t *)PSOC_STACK_END;
        
    // Fill test stack block with predefined pattern
    for (uint8_t i = 0; i < (STACK_TEST_BLOCK_SIZE / sizeof(uint16_t)); i++)
    {
        *stack = STACK_TEST_PATTERN;
        stack++;
    }
}
