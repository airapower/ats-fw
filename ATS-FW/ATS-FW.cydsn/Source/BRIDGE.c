/*******************************************************************************
           FILE:  BRIDGE.c

    DESCRIPTION:  BRIDGE module

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2021, Aira Inc.
        CREATED:  07/26/21
*******************************************************************************/

#include "BRIDGE_Private.h"
#include "LIN_MASTER_Public.h"

#include "project.h"

/*******************************************************************************
    FUNCTION: BRIDGE_Start

    SUMMARY:
    Starts the BRIDGE module.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void BRIDGE_Start(void)
{
    RB_Init(&bridge.from_host, bridge.from_host_rb_buffer, sizeof(bridge.from_host_rb_buffer));
    RB_Init(&bridge.to_host, bridge.to_host_rb_buffer, sizeof(bridge.to_host_rb_buffer));

    STATE_MACHINE_InitStateMachine(&bridge.state, STATE_MACHINE_ID_BRIDGE, BRIDGE_STATE_WAITING);
}

/*******************************************************************************
    FUNCTION: BRIDGE_Service
    
    SUMMARY:
    Services the BRIDGE FSM.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void BRIDGE_Service(void)
{
    if (bridge.state.current_state == BRIDGE_STATE_WAITING && BRIDGE_FromHostBytesReadyToTransmit())
    {
        STATE_MACHINE_ChangeState(&bridge.state, BRIDGE_STATE_TRANSMITTING, BRIDGE_REASON_DEQUEUED_FROM_HOST_MESSAGE, BRIDGE_FSM_MUTE);
    }
    else
    {
        return;
    }

    bool success;
    lin_master_msg_status_t status = LIN_MASTER_MessageStatus();

    switch (bridge.state.current_state)
    {
    case BRIDGE_STATE_TRANSMITTING:
        if (bridge.state.first_time_in_state)
        {
            bridge.working_buffer.bytes[0] = BRIDGE_LIN_COMS_SID;
            bridge.working_buffer.length = BRIDGE_FromHostDequeue(&bridge.working_buffer.bytes[1], BRIDGE_WORKING_BUFFER_SIZE - 1) + 1;
        }

        if (status == LIN_MASTER_IDLE)
        {
            success = LIN_MASTER_SendMessage(BRIDGE_LIN_SLAVE_NAD, bridge.working_buffer.bytes, bridge.working_buffer.length);
            if (success)
            {
                STATE_MACHINE_ChangeState(&bridge.state, BRIDGE_STATE_RECEIVING, BRIDGE_REASON_TRANSMITTED_TO_DUT_MESSAGE, BRIDGE_FSM_MUTE);
            }
            else
            {
                BRIDGE_ErrorHandler(BRIDGE_ERROR_TO_DUT_TRANSMIT);
                STATE_MACHINE_ChangeState(&bridge.state, BRIDGE_STATE_WAITING, BRIDGE_REASON_TRANSMITTED_TO_DUT_ERROR, BRIDGE_FSM_MUTE);
            }
        }
        break;

    case BRIDGE_STATE_RECEIVING:
        if (status == LIN_MASTER_COMPLETED)
        {
            success = LIN_MASTER_ReceiveMessage(bridge.working_buffer.bytes, &bridge.working_buffer.length, BRIDGE_WORKING_BUFFER_SIZE);
            if (success)
            {
                success = BRIDGE_ToHostEnqueue(&bridge.working_buffer.bytes[1], bridge.working_buffer.length - 1);
                if (success)
                {
                    STATE_MACHINE_ChangeState(&bridge.state, BRIDGE_STATE_TRANSMITTING, BRIDGE_REASON_RECEIVED_FROM_DUT_MESSAGE, BRIDGE_FSM_MUTE);
                }
                else
                {
                    BRIDGE_ErrorHandler(BRIDGE_ERROR_TO_HOST_OVERFLOW);
                    STATE_MACHINE_ChangeState(&bridge.state, BRIDGE_STATE_WAITING, BRIDGE_REASON_ENQUEUED_TO_HOST_ERROR, BRIDGE_FSM_MUTE);
                }
            }
            else
            {
                BRIDGE_ErrorHandler(BRIDGE_ERROR_FROM_DUT_RECEIVE);
                STATE_MACHINE_ChangeState(&bridge.state, BRIDGE_STATE_WAITING, BRIDGE_REASON_RECEIVED_FROM_DUT_ERROR, BRIDGE_FSM_MUTE);
            }
        }
        break;

    default:
        break;
    }
}

/*******************************************************************************
    FUNCTION: BRIDGE_ToHostBytesReadyToTransmit

    SUMMARY:
    Queries number of bytes ready to send up to the host in the uplink buffer.

    PARAMETERS:
    none

    RETURNS:
    number of bytes from the uplink buffer ready to transmit. 
*******************************************************************************/
uint32_t BRIDGE_ToHostBytesReadyToTransmit(void)
{
    uint8_t int_en = CyEnterCriticalSection();
    uint32_t ready = RB_Used(&bridge.to_host);
    CyExitCriticalSection(int_en);
    return ready;
}

/*******************************************************************************
    FUNCTION: BRIDGE_ToHostDequeue
    
    SUMMARY:
    Dequeues up max_len bytes from the uplink buffer

    PARAMETERS:
    buffer - the destination buffer for the bytes to dequeue from the uplink buffer
    length - the desired amount of bytes to dequeue from the uplink buffer

    RETURNS:
    number of bytes actually dequeued from the uplink buffer.
*******************************************************************************/
uint32_t BRIDGE_ToHostDequeue(uint8_t *buffer, uint32_t max_len)
{
    uint8_t int_en = CyEnterCriticalSection();
    uint32_t read = RB_Read(&bridge.to_host, buffer, max_len);
    CyExitCriticalSection(int_en);
    return read;
}

/*******************************************************************************
    FUNCTION: BRIDGE_FromHostEnqueue
    
    SUMMARY:
    Enqueues length bytes to the downlink buffer

    PARAMETERS:
    buffer - the source buffer for the bytes to enqeue into the downlink buffer
    length - the desired amount of bytes to enqeue into the downlink buffer

    RETURNS:
    true if length bytes were successfully written, false otherwise.
*******************************************************************************/
bool BRIDGE_FromHostEnqueue(const uint8_t *buffer, uint32_t length)
{
    uint8_t int_en = CyEnterCriticalSection();
    bool success = RB_Write(&bridge.from_host, buffer, length, false);
    CyExitCriticalSection(int_en);
    return success;
}

/*******************************************************************************
    FUNCTION: BRIDGE_FromHostBytesReadyToTransmit

    SUMMARY:
    Queries number of bytes ready to send dwon to the DUT in the downlink buffer.

    PARAMETERS:
    none

    RETURNS:
    number of bytes from the downlink buffer ready to transmit. 
*******************************************************************************/
static uint32_t BRIDGE_FromHostBytesReadyToTransmit(void)
{
    uint8_t int_en = CyEnterCriticalSection();
    uint32_t ready = RB_Used(&bridge.from_host);
    CyExitCriticalSection(int_en);
    return ready;
}

/*******************************************************************************
    FUNCTION: BRIDGE_ToHostEnqueue
    
    SUMMARY:
    Enqueues length bytes to the uplink buffer

    PARAMETERS:
    buffer - the source buffer for the bytes to enqeue into the uplink buffer
    length - the desired amount of bytes to enqeue into the uplink buffer

    RETURNS:
    true if length bytes were successfully written, false otherwise.
*******************************************************************************/
static bool BRIDGE_ToHostEnqueue(const uint8_t *buffer, uint32_t length)
{
    uint8_t int_en = CyEnterCriticalSection();
    bool success = RB_Write(&bridge.to_host, buffer, length, false);
    CyExitCriticalSection(int_en);
    return success;
}

/*******************************************************************************
    FUNCTION: BRIDGE_FromHostDequeue
    
    SUMMARY:
    Dequeues up max_len bytes from the downlink buffer

    PARAMETERS:
    buffer - the destination buffer for the bytes to dequeue from the downlink buffer
    length - the desired amount of bytes to dequeue from the downlink buffer

    RETURNS:
    number of bytes actually dequeued from the downlink buffer.
*******************************************************************************/
static uint32_t BRIDGE_FromHostDequeue(uint8_t *buffer, uint32_t max_length)
{
    uint8_t int_en = CyEnterCriticalSection();
    uint32_t read = RB_Read(&bridge.from_host, buffer, max_length);
    CyExitCriticalSection(int_en);
    return read;
}

/*******************************************************************************
    FUNCTION: BRIDGE_ErrorHandler
    
    SUMMARY:
    Centralized error handler for the BRIDGE FSM.

    PARAMETERS:
    error - the error to handle

    RETURNS:
    none
*******************************************************************************/
static void BRIDGE_ErrorHandler(bridge_error_t error)
{
    switch (error)
    {
    //TODO(jules) debug printouts?
    default:
        break;
    }
}
