/*******************************************************************************
           FILE:  SYSTIMER_Private.h

    DESCRIPTION:  System timing API.

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  11/4/19
*******************************************************************************/
#ifdef SYSTIMER_PRIVATE_H
#error "SYSTIMER_Private.h was incorrectly included in " __FILE__
#else
#define SYSTIMER_PRIVATE_H
#endif

#include <stdint.h>
#include "SYSTIMER_Public.h"

//Private defines, enums, typedefs, etc.

#define SYSTIMER_MSEC_RIGHT_BITS    6                                                       //2^6 == 64 which is used to convert the system clock to a 1kHz clock
#define SYSTIMER_MSEC_LEFT_BITS     (32 - SYSTIMER_MSEC_RIGHT_BITS)                         //available most-significant bits after the LSBs are shifted out
#define SYSTIMER_MSEC_FINAL_DIVISOR (SYSTIMER_UNITS_MS / (1 << SYSTIMER_MSEC_RIGHT_BITS))   //final divisor used to convert the system clock to a 1kHz clock

typedef struct 
{
    uint16_t roll_over_event_counter;
    uint32_t previous_time_41p666ns;
} systimer_context_t;

//Private Variables

static systimer_context_t systimer_context;

//Private Prototypes

static uint32_t SYSTIMER_UpdateTime_41p666nS(void);
static uint32_t SYSTIMER_GetElapsedTime_41p666nS(uint32_t start_time_41p666ns);
static uint32_t SYSTIMER_GetTime_41p666nS(void);
