/*******************************************************************************
           FILE:  RB_Public.h

    DESCRIPTION:  Generic FIFO ring buffer API

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2020, Aira Inc.
        CREATED:  10/22/20
*******************************************************************************/

#ifndef RB_PUBLIC_H
#define RB_PUBLIC_H

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
    uint8_t *buffer;
    uint32_t head;
    uint32_t tail;
    uint32_t max_len;
    bool is_full;
} RB_t;

void RB_Init(RB_t *rb, uint8_t *buffer, uint32_t buffer_len);
void RB_Reset(RB_t *rb);

uint32_t RB_Read(RB_t *rb, uint8_t *dst, uint32_t max_len);
uint32_t RB_Peek(RB_t *rb, uint8_t *dst, uint32_t max_len);
bool RB_Write(RB_t *rb, const uint8_t *src, uint32_t len, bool overwrite);

uint32_t RB_Used(RB_t *rb);
uint32_t RB_Available(RB_t *rb);

#endif /* RB_PUBLIC_H */
