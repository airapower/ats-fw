/*******************************************************************************
           FILE:  LIN_HAL_Private.h

    DESCRIPTION: LIN HAL Private API

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2021, Aira Inc.
        CREATED:  06/29/21
*******************************************************************************/
#ifdef LIN_HAL_PRIVATE_H
#error "LIN_HAL_Private.h was incorrectly included in " __FILE__
#else
#define LIN_HAL_PRIVATE_H
#endif

#include "LIN_HAL_Public.h"

#include "project.h"

//Private defines, enums, typedefs, etc.
#define LIN_HAL_BREAK_SYNC_DELAY_US (250u)  // Delay between break field and sync byte

//Private Variables
static bool rx_is_enabled = false; // Used to keep track of reception state

//Private Prototypes
CY_ISR_PROTO(LIN_HAL_ISR);
