/*******************************************************************************
           FILE:  RB.c

    DESCRIPTION:  Generic FIFO ring buffer API implementation

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2020, Aira Inc.
        CREATED:  10/22/20
*******************************************************************************/
#include <string.h>

#include "common.h"
#include "RB_Public.h"

/*******************************************************************************
    FUNCTION: RB_Init

    SUMMARY:
    Initializes the ring buffer. To be called only once during startup.
    RB_Reset() should be called instead to reset the state of the buffer
    as needed.

    PARAMETERS:
    rb          - pointer to the ring buffer struct to initialize
    buffer      - pointer to the raw memory buffer to be used to store ring buffer
                  items
    buffer_len  - length of entire buffer

    RETURNS:
    none
*******************************************************************************/
void RB_Init(RB_t *rb, uint8_t *buffer, uint32_t buffer_len)
{
    rb->buffer = buffer;
    rb->max_len = buffer_len;
    rb->head = 0;
    rb->tail = 0;
    rb->is_full = false;
}

/*******************************************************************************
    FUNCTION: RB_Reset

    SUMMARY:
    Resets the state of the ring buffer such that it appears empty.

    PARAMETERS:
    rb          - pointer to the ring buffer struct

    RETURNS:
    none
*******************************************************************************/
void RB_Reset(RB_t *rb)
{
    rb->head = 0;
    rb->tail = 0;
    rb->is_full = false;
}

/*******************************************************************************
    FUNCTION: RB_Read

    SUMMARY:
    Reads up to max_len bytes from the ring buffer into the dst buffer.

    PARAMETERS:
    rb      - pointer to the ring buffer struct
    dst     - pointer to memory to fill with the read data
    max_len - max number of bytes to read

    RETURNS:
    number of bytes read from the ring buffer
*******************************************************************************/
uint32_t RB_Read(RB_t *rb, uint8_t *dst, uint32_t max_len)
{
    max_len = MIN(RB_Used(rb), max_len);

    if (max_len < 1)
    {
        // RB is empty
        return 0;
    }

    if (rb->is_full)
    {
        rb->is_full = false;
    }

    uint32_t next = rb->tail + max_len;

    if (next >= rb->max_len)
    {
        /* Handle wrap-around
           move the first part of the item */
        uint32_t n = rb->max_len - rb->tail;
        uint32_t m = next - rb->max_len;

        memcpy(dst, rb->buffer + rb->tail, n);
        memcpy(dst + n, rb->buffer, m);
        next = m;
    }
    else
    {
        memcpy(dst, rb->buffer + rb->tail, max_len);
    }

    rb->tail = next;
    return max_len;
}

/*******************************************************************************
    FUNCTION: RB_Peek

    SUMMARY:
    Peeks up to max_len bytes from the ring buffer into the dst buffer.

    PARAMETERS:
    rb      - pointer to the ring buffer struct
    dst     - pointer to memory to fill with the peeked item
    max_len - max number of bytes to peek

    RETURNS:
    number of bytes peeked from the ring buffer
*******************************************************************************/
uint32_t RB_Peek(RB_t *rb, uint8_t *dst, uint32_t max_len)
{
    max_len = MIN(RB_Used(rb), max_len);

    if (max_len < 1)
    {
        // RB is empty
        return 0;
    }

    uint32_t next = rb->tail + max_len;

    if (next >= rb->max_len)
    {
        // Handle wrap-around
        uint32_t n = rb->max_len - rb->tail;
        uint32_t m = next - rb->max_len;

        memcpy(dst, rb->buffer + rb->tail, n);
        memcpy(dst + n, rb->buffer, m);
        next = m;
    }
    else
    {
        memcpy(dst, rb->buffer + rb->tail, max_len);
    }

    return max_len;
}

/*******************************************************************************
    FUNCTION: RB_Write

    SUMMARY:
    Writes an item onto the ring buffer. If overwrite is enabled, overwrites
    the oldest item in the ring buffer even if buffer is full.

    PARAMETERS:
    rb        - pointer to the ring buffer struct
    src       - pointer to memory to read from
    len       - number of bytes to write
    overwrite - whether overwriting is allowed if buffer is full

    RETURNS:
    true if item was written successfully (always true if overwrite is enabled)
*******************************************************************************/
bool RB_Write(RB_t *rb, const uint8_t *src, uint32_t len, bool overwrite)
{
    uint32_t remaining = RB_Available(rb);

    if (remaining < len)
    {
        if (overwrite)
        {
            rb->tail += len;
            if (rb->tail > rb->max_len)
            {
                rb->tail -= rb->max_len;
            }
        }
        else
        {
            // Not enough space & No overwriting
            return false;
        }
    }

    if (remaining <= len)
    {
        rb->is_full = true;
    }

    uint32_t next = rb->head + len;

    if (next >= rb->max_len)
    {
        // Handle wrap-around
        uint32_t n = rb->max_len - rb->head;
        uint32_t m = next - rb->max_len;

        memcpy(rb->buffer + rb->head, src, n);
        memcpy(rb->buffer, src + n, m);
        next = m;
    }
    else
    {
        memcpy(rb->buffer + rb->head, src, len);
    }

    rb->head = next;
    return true;
}

/*******************************************************************************
    FUNCTION: RB_Used

    SUMMARY:
    Returns the number of data bytes currently in the buffer. 

    PARAMETERS:
    rb       - pointer to the ring buffer struct

    RETURNS:
    the number of data bytes currently in the buffer
*******************************************************************************/
uint32_t RB_Used(RB_t *rb)
{
   uint32_t used;

   if (rb->tail < rb->head)
   {
       used = rb->head - rb->tail;
   }
   else if (rb->tail > rb->head)
   {
       used = rb->max_len - rb->tail + rb->head;
   }
   else
   {
       used = rb->is_full ? rb->max_len : 0;
   }

    return used;
}

/*******************************************************************************
    FUNCTION: RB_Available

    SUMMARY:
    Returns the number of unused bytes currently in the buffer. 

    PARAMETERS:
    rb       - pointer to the ring buffer struct

    RETURNS:
    the number of unused bytes currently in the buffer
*******************************************************************************/
uint32_t RB_Available(RB_t *rb)
{
    uint32_t available;

    if (rb->tail < rb->head)
    {
        available = rb->max_len - rb->head + rb->tail;
    }
    else if (rb->tail > rb->head)
    {
        available = rb->tail - rb->head;
    }
    else
    {
        available = rb->is_full ? 0 : rb->max_len;
    }

    return available;
}
