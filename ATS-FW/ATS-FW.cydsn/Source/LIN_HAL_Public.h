/*******************************************************************************
           FILE:  LIN_HAL_Public.h

    DESCRIPTION:  LIN HAL Public API

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2021, Aira Inc.
        CREATED:  06/29/2021
*******************************************************************************/
#ifndef LIN_HAL_PUBLIC_H
#define LIN_HAL_PUBLIC_H

#include <stdbool.h>

#include "LIN_NETWORK_Public.h"

void LIN_HAL_Init(void);
bool LIN_HAL_SetEnableReceiving(bool enable);
bool LIN_HAL_SendFrameHeader(const lin_slot_t *slot);
bool LIN_HAL_SendFrameData(const lin_slot_t *slot);
bool LIN_HAL_IsDoneSending(void);

#endif // LIN_HAL_PUBLIC_H
