/*******************************************************************************
           FILE:  SYSTIMER.c

    DESCRIPTION:  System timing API.

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  11/4/19
*******************************************************************************/
#include "SYSTIMER_Public.h"
#include "SYSTIMER_Private.h"
#include "project.h"

/*******************************************************************************
    FUNCTION: SYSTIMER_Start

    SUMMARY:
    This function initializes the system timer module.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void SYSTIMER_Start(void)
{
    SYSTEM_TIMER_Start();

    systimer_context.roll_over_event_counter = 0;
    systimer_context.previous_time_41p666ns = SYSTIMER_GetElapsedTime_41p666nS(0);
}

/*******************************************************************************
    FUNCTION: SYSTIMER_Service

    SUMMARY:
    This function services and updates the system timer module. 

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void SYSTIMER_Service(void)
{
    SYSTIMER_UpdateTime_41p666nS();
}

/*******************************************************************************
    FUNCTION: SYSTIMER_GetSysTime_mS

    SUMMARY:
    This function converts the system timer counter to a 1-msec counter.
    It divides the counter by 24,000 to simulate a counter with a 1 kHz clock
    input.

    PARAMETERS:
    none

    RETURNS:
    current system timer counter value in mS units
*******************************************************************************/
uint32_t SYSTIMER_GetSysTime_mS(void)
{
    uint64_t current_time_ms;
    uint64_t time_41p666nS = SYSTIMER_UpdateTime_41p666nS();

    /* The system timer counter is divided by 24000 in order to generate a
       1mS counter.  But we would like to maximize the range of the possible
       output before a roll-over would occur.  Hence we will add back in the
       system timer roll-over events which effectively extends the 32-bit
       system timer counter to 48 bits (i.e., 32-bit counter plus 16-bit
       roll-over event counter).

       So, the 24000 divisor will be split into two operations.  The first
       discards the least-significant bits that still allows for an integral
       amount of time.  The greatest power-of-2 factor of 24000 is 2^6 = 64:
       24000 / 64 = 375.

       So the final calculation is composed of the current 32-bit system timer
       count value shifted to the right by 6 bits (i.e., realizing the
       divide-by-64).  At the same time the roll-over event bits are shifted
       into the MSBs of the 32-bit count value, thereby recovering dynamic
       range. The final result is divided by 375 which completes the "division-
       by-24000" operation to determine the current time in msec. */
    current_time_ms = (time_41p666nS >> SYSTIMER_MSEC_RIGHT_BITS) |
                      (((uint64_t) systimer_context.roll_over_event_counter) << SYSTIMER_MSEC_LEFT_BITS);
                    
    return (uint32_t)(current_time_ms / SYSTIMER_MSEC_FINAL_DIVISOR);
}

/*******************************************************************************
    FUNCTION: SYSTIMER_StopTimer

    SUMMARY:
    Stops a timer.

    PARAMETERS:
    timer - the timer object

    RETURNS:
    none
*******************************************************************************/
void SYSTIMER_StopTimer(systimer_t* timer)
{
    timer->running = false;
}

/*******************************************************************************
    FUNCTION: SYSTIMER_AddTime

    SUMMARY:
    Add time to an already running timer

    PARAMETERS:
    timer - the timer object
    time_to_add_xs - the time to add in timer's units

    RETURNS:
    true if time was added, false otherwise
*******************************************************************************/
bool SYSTIMER_AddTime(systimer_t* timer, uint32_t time_to_add_xs)
{
    if(SYSTIMER_IsExpired(timer))
    {
        return false;
    }

    uint32_t time_remaining_xs = SYSTIMER_GetRemaining_xS(timer);
    uint64_t new_period_xs = (uint64_t)time_remaining_xs + (uint64_t)time_to_add_xs;
    uint64_t new_period_41p666ns = new_period_xs * (uint64_t)timer->units;

    if(new_period_41p666ns > SYSTIMER_MAX_PERIOD_TICKS)
    {
        return false;
    }

    timer->start_time_41p666nS = SYSTIMER_GetTime_41p666nS();
    timer->period_xs = (uint32_t)new_period_xs;
    timer->running = true;

    return true;
}

/*******************************************************************************
    FUNCTION: SYSTIMER_GetRemaining_xS

    SUMMARY:
    Returns the remaining time of a running timer. Note that the time remaining 
    is returned in the units set for the timer object.

    PARAMETERS:
    timer - the timer object

    RETURNS:
    the time remaining before expiring in timer's units
*******************************************************************************/
uint32_t SYSTIMER_GetRemaining_xS(systimer_t* timer)
{
    uint32_t time_remaining_xs = 0;
    uint32_t time_elapsed_xs;
    if(timer->running)
    {
        time_elapsed_xs = SYSTIMER_GetElapsed_xS(timer);
        
        //if the elapsed time has exceed the period, we should return zero
        if(time_elapsed_xs >= timer->period_xs)
        {
            return time_remaining_xs;
        }
        
        time_remaining_xs = timer->period_xs - time_elapsed_xs;
    }

    return time_remaining_xs;
}

/*******************************************************************************
    FUNCTION: SYSTIMER_GetElapsed_xS

    SUMMARY:
    Returns the elapsed time since the timer was started

    PARAMETERS:
    timer - the timer object

    RETURNS:
    the time since the timer was started in timer's units
*******************************************************************************/
uint32_t SYSTIMER_GetElapsed_xS(systimer_t* timer)
{
    uint32_t time_elapsed_xs = 0;

    if(timer->running)
    {
        time_elapsed_xs = SYSTIMER_GetElapsedTime_41p666nS(timer->start_time_41p666nS) / timer->units;

        //if the elapsed time has exceed the period, we should set the timer flag to stopped
        if(time_elapsed_xs >= timer->period_xs)
        {
            timer->running = false;
        }
    }

    return time_elapsed_xs;
}

/*******************************************************************************
    FUNCTION: SYSTIMER_StartTimer

    SUMMARY:
    Starts a timer object with specified units and period.

    PARAMETERS:
    timer - the timer object
    period_xs - the period that the timer will run for in timer's units
    units - the units that the timer will use

    RETURNS:
    true if timer was started, false otherwise
*******************************************************************************/
bool SYSTIMER_StartTimer(systimer_t* timer, uint32_t period_xs, systimer_units_t units)
{
    //make sure that the period is in an acceptable range for the timer
    uint64_t period_41p666ns = (uint64_t)period_xs * (uint64_t)units;

    if(period_41p666ns < SYSTIMER_MAX_PERIOD_TICKS)
    {
        timer->start_time_41p666nS = SYSTIMER_GetTime_41p666nS();
        timer->running = true;
        timer->period_xs = period_xs;
        timer->units = units;
        return true;
    }

    return false;
}

/*******************************************************************************
    FUNCTION: SYSTIMER_IsExpired

    SUMMARY:
    Check to see if a timer has expired (counted past its set period)

    PARAMETERS:
    timer - the timer object

    RETURNS:
    true if timer has expired, false otherwise
*******************************************************************************/
bool SYSTIMER_IsExpired(systimer_t* timer)
{
    if(SYSTIMER_GetRemaining_xS(timer) == 0)
    {
        timer->running = false;
        return true;
    }
    else
    {
        return false;
    }
}

/*******************************************************************************
    FUNCTION: SYSTIMER_UpdateTime_41p666nS

    SUMMARY:
    The system timer's counter will roll over about every 178.95697Sec; this
    function increments a 16-bit value capturing the roll-over event -- hence
    it only needs to be called within the 178 second upper ceiling.

    PARAMETERS:
    none

    RETURNS:
    current timer count value in 41.666 nS units
*******************************************************************************/
static uint32_t SYSTIMER_UpdateTime_41p666nS(void)
{
    uint32_t time_41p666ns = SYSTIMER_GetElapsedTime_41p666nS(0);
    if (systimer_context.previous_time_41p666ns > time_41p666ns)
    {
        systimer_context.roll_over_event_counter++;
    }
    systimer_context.previous_time_41p666ns = time_41p666ns;

    return time_41p666ns;
}

/*******************************************************************************
    FUNCTION: SYSTIMER_GetElapsedTime_41p666nS

    SUMMARY:
    This function returns the elapsed time from a given start time. This
    function accounts for the system timer counter wrap-around event.
    Max time interval: 4,294,967,296‬ Ticks (32Bit), ~178.95697Sec

    PARAMETERS:
    start_time_41p666ns - starting time in 41.666 nS units

    RETURNS:
    elapsed time since the provided start time in 41.666 nS units
*******************************************************************************/
static uint32_t SYSTIMER_GetElapsedTime_41p666nS(uint32_t start_time_41p666ns)
{
    /* Because data types are the same size and unsigned, in an overflow case
       two's complement handles the overflow case without issue. It should also
       be noted that if the time is not checked within the max timing period,
       the delta time will not be correctly calculated. */
    return (start_time_41p666ns - SYSTIMER_GetTime_41p666nS());
}

/*******************************************************************************
    FUNCTION: SYSTIMER_GetTime_41p666nS

    SUMMARY:
    This function calls the PSoC API "Timer_ReadCounter()" associated with the
    top level SYSTEM_TIMER block.  From the datasheet, this "forces a capture
    and then returns the capture value."  Also from the datasheet's General
    Description the count mode is "Down only".  The top level has a 24MHz
    clock as the timer's input, so the return value is in ticks of a 24MHz
    clock so each tick is 41.666 nS.

    PARAMETERS:
    none

    RETURNS:
    current timer count value in 41.666 nS units
*******************************************************************************/
static uint32_t SYSTIMER_GetTime_41p666nS(void)
{
    return SYSTEM_TIMER_ReadCounter();
}
