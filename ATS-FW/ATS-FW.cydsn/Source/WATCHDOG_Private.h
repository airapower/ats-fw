/*******************************************************************************
           FILE:  WATCHDOG_Private.h

    DESCRIPTION:  APIs for controlling and servicing the watchdog timer

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  12/11/19
*******************************************************************************/
#ifdef WATCHDOG_PRIVATE_H
#error "WATCHDOG_Private.h was incorrectly included in " __FILE__
#else
#define WATCHDOG_PRIVATE_H
#endif

#include <stdbool.h>

//Private defines, enums, typedefs, etc.

#define WATCHDOG_ENABLED       //Comment out to disable the watchdog

//Note: Match values are typically cycles of the 32KHz LFCLK (31.25uS) +/-60%
                                            //WDT0(master interrupt) master WDT timout period (default:   128  or   4mS)
#define WATCHDOG_COUNT0_MATCH       16000   //WDT0(master interrupt) master WDT timout period (setting: 16000  or 500mS)

//Private Variables
