/*******************************************************************************
           FILE:  LIN_HAL.c

    DESCRIPTION:  LIN HAL module

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2021, Aira Inc.
        CREATED:  06/29/21
*******************************************************************************/

#include "LIN_HAL_Private.h"
#include "LIN_MASTER_Public.h"

#include "LIN_MASTER_HW.h"
#include "LIN_MASTER_HW_SPI_UART.h"
#include "LIN_MASTER_HW_SCB_IRQ.h"

/*******************************************************************************
    FUNCTION: LIN_HAL_Init

    SUMMARY:
    Initializes the hardware for the LIN MASTER module. 

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void LIN_HAL_Init(void)
{
    LIN_MASTER_HW_Init();
    LIN_MASTER_HW_Enable();
    LIN_MASTER_HW_SCB_IRQ_ClearPending();
    LIN_MASTER_HW_SCB_IRQ_StartEx(&LIN_HAL_ISR);
    LIN_MASTER_HW_DisableInt();
}

/*******************************************************************************
    FUNCTION: LIN_HAL_SetEnableReceiving

    SUMMARY:
    Enables or disables the reception of bytes from the LIN RX

    PARAMETERS:
    enable - the desired state of reception

    RETURNS:
    true if receiving state is set to enable.
*******************************************************************************/
bool LIN_HAL_SetEnableReceiving(bool enable)
{
    if(enable != rx_is_enabled)
    {
        if(enable)
        {
            LIN_MASTER_HW_SpiUartClearRxBuffer();
            LIN_MASTER_HW_EnableInt();
        }
        else
        {
            LIN_MASTER_HW_DisableInt();
        }

        rx_is_enabled = enable;
    }

    return true;
}

/*******************************************************************************
    FUNCTION: LIN_HAL_SendFrameHeader

    SUMMARY:
    Sends the LIN frame header for the given LIN slot. This is a partially blocking
    call: the function blocks for the transmission of the break signal and for the
    enqueuing of the sync & PID bytes, but does not wait for transmission to finish.

    PARAMETERS:
    slot - the slot who's frame header should be sent

    RETURNS:
    true if frame header has been correctly enqueued for transmission.
*******************************************************************************/
bool LIN_HAL_SendFrameHeader(const lin_slot_t *slot)
{
    LIN_MASTER_HW_UartSendBreakBlocking(LIN_BREAK_BITS_COUNT);
    CyDelayUs(LIN_HAL_BREAK_SYNC_DELAY_US);
    LIN_MASTER_HW_SpiUartWriteTxData(LIN_SYNC_FIELD_VALUE);
    uint8_t protected_id = LIN_MASTER_FID_to_PID(slot->id);
    LIN_MASTER_HW_SpiUartWriteTxData(protected_id);
    return true;
}

/*******************************************************************************
    FUNCTION: LIN_HAL_SendFrameData

    SUMMARY:
    Sends the LIN frame data for the given LIN slot. This is a partially blocking
    call: the function blocks for the enqueuing of the data & checksum bytes, but 
    does not wait for transmission to finish.

    PARAMETERS:
    slot - the slot who's frame data should be sent

    RETURNS:
    true if frame data has been correctly enqueued for transmission.
*******************************************************************************/
bool LIN_HAL_SendFrameData(const lin_slot_t *slot)
{
    LIN_MASTER_HW_SpiUartPutArray(slot->data, slot->length);
    uint8_t checksum = LIN_MASTER_CalculateChecksum(slot, slot->data);
    LIN_MASTER_HW_SpiUartWriteTxData(checksum);
    return true;
}

/*******************************************************************************
    FUNCTION: LIN_HAL_SendFrameData

    SUMMARY:
    Checks the state of the bytes queued for transmission on the LIN bus.

    PARAMETERS:
    none

    RETURNS:
    true if all queued LIN transmissions are finished
*******************************************************************************/
bool LIN_HAL_IsDoneSending(void)
{
    bool done = false;
    if(LIN_MASTER_HW_INTR_TX_REG & LIN_MASTER_HW_INTR_TX_UART_DONE)
    {
        // clear TX done bit (w1c register)
        LIN_MASTER_HW_INTR_TX_REG = LIN_MASTER_HW_INTR_TX_UART_DONE;
        done = true;
    }

    return done;
}

/*******************************************************************************
    FUNCTION: LIN_HAL_ISR

    SUMMARY:
    ISR that feeds received bytes to the LIN MASTER module.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
CY_ISR(LIN_HAL_ISR)
{
    // Dump all RX data whenever TX is done to ignore loopback data
    if(LIN_MASTER_HW_INTR_TX_REG & LIN_MASTER_HW_INTR_TX_UART_DONE)
    {
        LIN_MASTER_HW_CLEAR_RX_FIFO;
        LIN_MASTER_HW_INTR_TX_REG = LIN_MASTER_HW_INTR_TX_UART_DONE;
        return;
    }

    // Keep grabbing bytes off the FIFO until we've gotten the whole frame
    if((LIN_MASTER_HW_INTR_RX_REG & LIN_MASTER_HW_INTR_RX_NOT_EMPTY) != 0)
    {
        // clear rx not empty bit (w1c register)
        LIN_MASTER_HW_INTR_RX_REG = LIN_MASTER_HW_INTR_RX_NOT_EMPTY | LIN_MASTER_HW_INTR_RX_FULL;

        bool pending = true;
        while(pending && LIN_MASTER_HW_SpiUartGetRxBufferSize())
        {
            uint8_t rx_byte = (uint8_t) LIN_MASTER_HW_SpiUartReadRxData();
            pending = LIN_MASTER_HandleReceivedByte(rx_byte);
        }

        if(pending == false)
        {
            LIN_MASTER_HW_DisableInt();
        }
    }
}