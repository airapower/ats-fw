/*******************************************************************************
           FILE:  USB_Private.h

    DESCRIPTION:  USB-to-PC interface manager.

         AUTHOR:  David Russell
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  11/18/19
*******************************************************************************/
#ifdef USB_PRIVATE_H
#error "USB_Private.h was incorrectly included in " __FILE__
#else
#define USB_PRIVATE_H
#endif

#include <stdint.h>
#include <stdbool.h>
#include "STATE_MACHINE_Public.h"
#include "USB_Public.h"

#include "project.h"

//Private defines, enums, typedefs, etc.

#define USB_FSM_UART_MUTE       (true)

#define USB_DEVICE              0       // device number of the desired device descriptor 
#define USB_MAX_PACKET_SIZE     64      // maximum USB packet size

/* USB full-speed frames occur at 1msec intervals with a rate of 12Mbps;
   this leads to an upper-bound on our FIFO size -- assuming our FIFO will
   load all packets into a single frame (empirically this seemed to be the
   case with the device connected straight to a computer; i.e., no hub allowing
   sharing of frame bandwidth).

   12Mbps / 8bpB = 1.5MBps
   1.5MBps / 1000 msec/sec = 1.5kB

   Update: reducing FIFO size due to limited SRAM -- this can be increased
   later if overflow is seen. */
#define USB_FIFO_BYTE_SIZE      250

typedef enum
{
    // Receive
    USB_EVENT_FLAG_PENDING_OUT_EP               = (1 << 0), // OUT_EP has been copied by the DMA in the driver

    // Transmit
    USB_EVENT_FLAG_TX_FINAL_PACKET              = (1 << 1), // The most recent transmitted packet is the last one of the frame
    USB_EVENT_FLAG_TX_FINAL_PACKET_COMPLETE     = (1 << 2), // The last transmitted packet of the frame has been sent
    USB_EVENT_FLAG_TX_EXPECTING_RESPONSE        = (1 << 3), // The IN_EP has been loaded, so allow the ISR to process the queue

    // Misc
    USB_EVENT_FLAG_ENTER_BOOTLOAD_MODE          = (1 << 4), // Reset the processor into bootloader mode when ready

    USB_EVENT_FLAG_ALL                          = (0x1F),   // <- Update if flags are changed
} event_flags_t;

typedef struct
{
    cyisraddress cy_med_isr;
    cyisraddress cy_hi_isr;

    state_machine_t state;
    volatile uint8_t event_flags;

    volatile uint8_t receive_endpoint_buffer[USB_MAX_PACKET_SIZE];
    volatile uint8_t transmit_endpoint_buffer[USB_MAX_PACKET_SIZE];
} usb_context_t;

//Private Variables

static usb_context_t usb_context;

//Private Prototypes

static CY_ISR_PROTO(USB_IntrMedIsr);
static CY_ISR_PROTO(USB_IntrHiIsr);

static bool USB_CheckEventFlags(uint8_t mask);
static void USB_ChangeEventFlags(uint8_t set_mask, uint8_t clear_mask);

static void USB_ResetStateHandler(void);
static void USB_IdleStateHandler(void);
static void USB_ReceivingStateHandler(void);
static void USB_TransmittingStateHandler(void);

static void USB_EnqueueOutEpPacket(void);
static void USB_StartEnqueueingInEpPackets(void);
static void USB_EnqueueInEpPacket(void);
