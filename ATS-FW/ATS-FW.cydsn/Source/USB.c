/*******************************************************************************
           FILE:  USB.c

    DESCRIPTION:  USB-to-PC interface manager.

         AUTHOR:  David Russell
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  11/18/19
*******************************************************************************/

#include "BRIDGE_Public.h"
#include "USB_Private.h"

#include "debug.h"

/*******************************************************************************
    FUNCTION: USB_Start

    SUMMARY:
    This function initializes the USB API.  After the USB device is started
    intercept the medium- and high-level interrupts.  This order matters
    because the Start() call performs the library's ISR registration.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void USB_Start(void)
{
    usb_context.event_flags = 0;
    STATE_MACHINE_InitStateMachine(&usb_context.state, STATE_MACHINE_ID_USB, USB_STATE_UNCONFIGURED);

    USBFS_Start(USB_DEVICE, USBFS_5V_OPERATION);

    /* The call to USBFS_Start will upgrade the priority of the DMA interrupt to 0 when:
     * - the endpoint is DMA managed
     * - the chip is PSOC4 family
     * - the endpoint management is automatic
     * 
     * see USBFS.c:543-560 for details.
     * 
     * This upgraded priority causes the ADC_DST DMA isr (25us~28us) to block the whole
     * system when it fires. This is not acceptable when Gen1 style analog ping is used.
     * Since the AP ISR is at priority 1, we downgrade the DMA ISR priority to 2.
     */
    CyIntSetPriority(CYDMA_INTR_NUMBER, 2);

    usb_context.cy_hi_isr = CyIntSetVector(USBFS_INTR_HI_VECT_NUM, &USB_IntrHiIsr);
    usb_context.cy_med_isr = CyIntSetVector(USBFS_INTR_MED_VECT_NUM, &USB_IntrMedIsr);
}

/*******************************************************************************
    FUNCTION: USB_Service

    SUMMARY:
    This function services and updates USB communications with the PC

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void USB_Service(void)
{
    if (USBFS_GetConfiguration())
    {
        if (USBFS_IsConfigurationChanged())
        {
            STATE_MACHINE_ChangeState(&usb_context.state, USB_STATE_RESET, USB_CONFIGURATION_CHANGED_EXIT_REASON, USB_FSM_UART_MUTE);
        }

        switch (usb_context.state.current_state)
        {
            case USB_STATE_UNCONFIGURED:
                break;

            case USB_STATE_RESET:
                USB_ResetStateHandler();
                break;

            case USB_STATE_IDLE:
                USB_IdleStateHandler();
                break;

            case USB_STATE_RECEIVING:
                USB_ReceivingStateHandler();
                break;

            case USB_STATE_TRANSMITTING:
                USB_TransmittingStateHandler();
                break;

            default:
                STATE_MACHINE_ChangeState(&usb_context.state, USB_STATE_IDLE, USB_INVALID_STATE_EXIT_REASON, false);
                break;
        }
    }
}

/*******************************************************************************
    FUNCTION: USB_EnterBootloadModeWhenReady

    SUMMARY:
    This function triggers the USB module to reset the processor and enter
    Bootloader HostLink mode that will wait "forever" (i.e., until the host
    sends a new bootloadable image or power is removed).  It sets an event flag
    so the USB module has a chance to finish transmitting any remaining response
    messages before the reset occurs.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void USB_EnterBootloadModeWhenReady(void)
{
    USB_ChangeEventFlags(USB_EVENT_FLAG_ENTER_BOOTLOAD_MODE, 0);
}

/*******************************************************************************
    FUNCTION: USB_Configured

    SUMMARY:
    This function returns true if the USB has been configured which implies
    it has one or more endpoints connected to a host.

    PARAMETERS:
    none

    RETURNS:
    true if the state is not unconfigured, false otherwise
*******************************************************************************/
bool USB_Configured(void)
{
    return usb_context.state.current_state == USB_STATE_UNCONFIGURED ? false : true;
}

/*******************************************************************************
    FUNCTION: USB_IntrMedIsr

    SUMMARY:
    This ISR is called in response to a medium-level USB interrupt, which
    includes all endpoint interrupts.  The default handler is called first, 
    then the IN endpoint state is checked to see if an IN transfer is completed;
    if so the IN endpoint buffer can be reloaded because the Host has read the
    previous data.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static CY_ISR(USB_IntrMedIsr)
{
    //Call the library's ISR first
    usb_context.cy_med_isr();

    //Check if the IN EP is now pending; if so process it.
    if (USBFS_GetEPState(USBFS_EP2) == USBFS_EVENT_PENDING)
    {
        if (USB_CheckEventFlags(USB_EVENT_FLAG_TX_EXPECTING_RESPONSE) == true)
        {
            USB_EnqueueInEpPacket();
        }
    }
}

/*******************************************************************************
    FUNCTION: USB_IntrHiIsr

    SUMMARY:
    This ISR is called in response to a high-level USB interrupt, which
    includes the Arbiter interrupt.  The default handler is called first, 
    then the OUT endpoint state is checked to see if an OUT DMA transfer has
    completed; if so the OUT endpoint data is ready to be read from the
    endpoint buffer.

    Note: There is a known issue whereby for OUT endpoints the endpoint event
    is not usable because the last chunk of data is still being copied.
    For CY_PSOC4: the Arbiter interrupt is used instead.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static CY_ISR(USB_IntrHiIsr)
{
    //Call the library's ISR first
    usb_context.cy_hi_isr();

    //Check if this is the Rx EP interrupt; if so process it.
    if (USBFS_GetEPState(USBFS_EP3) == USBFS_EVENT_PENDING)
    {
        /* The OUT_EP DMA transfer has completed.  Set a flag to tell the
           application it is safe to read from the DMA target buffer,
           receive_endpoint_buffer.  This buffer will not be written to
           again until the EP is re-armed to do so via USB_UART_EnableOutEP(). */
        USB_ChangeEventFlags(USB_EVENT_FLAG_PENDING_OUT_EP, 0);
    }
}

/*******************************************************************************
    FUNCTION: USB_CheckEventFlags

    SUMMARY:
    This function returns if the specified event flags are set or clear
    utilizing ISR protection.

    PARAMETERS:
    mask - desired flags to check

    RETURNS:
    true if all flags provided in the mask are set, false otherwise
*******************************************************************************/
static bool USB_CheckEventFlags(uint8_t mask)
{
    bool event_flags_set;
    uint8_t int_en = CyEnterCriticalSection();

    event_flags_set = ((usb_context.event_flags & mask) == mask) ? true : false;

    CyExitCriticalSection(int_en);

    return event_flags_set;
}

/*******************************************************************************
    FUNCTION: USB_ChangeEventFlags

    SUMMARY:
    This function sets or clears the specified event flags utilizing ISR
    protection.

    PARAMETERS:
    set_mask - desired flags to set
    clear_mask - desired flags to clear

    RETURNS:
    none
*******************************************************************************/
static void USB_ChangeEventFlags(uint8_t set_mask, uint8_t clear_mask)
{
    uint8_t int_en = CyEnterCriticalSection();

    usb_context.event_flags |= set_mask;
    usb_context.event_flags &= (uint8_t)~clear_mask;

    CyExitCriticalSection(int_en);
}

/*******************************************************************************
    FUNCTION: USB_ResetStateHandler

    SUMMARY:
    This function manages the reset state which is called whenever the low-level
    endpoint conditions become active.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void USB_ResetStateHandler(void)
{
    if (usb_context.state.first_time_in_state == FIRST_TIME_IN_STATE)
    {
        usb_context.state.first_time_in_state = !FIRST_TIME_IN_STATE;

        USB_ChangeEventFlags(0, USB_EVENT_FLAG_ALL);
        
        USBFS_ReadOutEP(USBFS_EP3, (uint8_t *)usb_context.receive_endpoint_buffer, USB_MAX_PACKET_SIZE); 
        USBFS_EnableOutEP(USBFS_EP3);
        USBFS_LoadInEP(USBFS_EP2, (uint8_t *)usb_context.transmit_endpoint_buffer, sizeof(usb_context.transmit_endpoint_buffer));
    }

    STATE_MACHINE_ChangeState(&usb_context.state, USB_STATE_IDLE, USB_START_IDLE_EXIT_REASON, true);
}

/*******************************************************************************
    FUNCTION: USB_IdleStateHandler

    SUMMARY:
    This function manages receive and transmit events given the USB state is
    idle.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void USB_IdleStateHandler(void)
{
    if (usb_context.state.first_time_in_state == FIRST_TIME_IN_STATE)
    {
        usb_context.state.first_time_in_state = !FIRST_TIME_IN_STATE;

        if (USB_CheckEventFlags(USB_EVENT_FLAG_ENTER_BOOTLOAD_MODE) == true)
        {
            #ifdef CY_BOOTLOADABLE_Bootloadable_H
                Bootloadable_Load();
                //Device reset here; no return to this point
            #endif
        }
    }

    if (USB_CheckEventFlags(USB_EVENT_FLAG_PENDING_OUT_EP) == true)
    {
        STATE_MACHINE_ChangeState(&usb_context.state, USB_STATE_RECEIVING, USB_START_RX_EXIT_REASON, true);
    }
}

/*******************************************************************************
    FUNCTION: USB_ReceivingStateHandler

    SUMMARY:
    This function manages receive and transmit events given the USB state is
    receiving.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void USB_ReceivingStateHandler(void)
{
    if (USB_CheckEventFlags(USB_EVENT_FLAG_PENDING_OUT_EP) == true)
    {
        USB_EnqueueOutEpPacket();
    }
    else
    {
        STATE_MACHINE_ChangeState(&usb_context.state, USB_STATE_TRANSMITTING, USB_START_RX_EXIT_REASON, USB_FSM_UART_MUTE);
    }
}

/*******************************************************************************
    FUNCTION: USB_TransmittingStateHandler

    SUMMARY:
    This function manages receive and transmit events given the USB state is
    transmitting.

    Note: the ISR repeatedly calls loading IN EP to perform the actual
    transmission

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void USB_TransmittingStateHandler(void)
{
    if (usb_context.state.first_time_in_state == FIRST_TIME_IN_STATE)
    {
        usb_context.state.first_time_in_state = !FIRST_TIME_IN_STATE;

        if(BRIDGE_ToHostBytesReadyToTransmit() > 0)
        {
            USB_StartEnqueueingInEpPackets();
        }
        else
        {
            STATE_MACHINE_ChangeState(&usb_context.state, USB_STATE_IDLE, USB_PROCESSING_FAILED_EXIT_REASON, USB_FSM_UART_MUTE);
            return;
        }
    }

    if (USB_CheckEventFlags(USB_EVENT_FLAG_TX_FINAL_PACKET_COMPLETE) == true)
    {
        STATE_MACHINE_ChangeState(&usb_context.state, USB_STATE_RECEIVING, USB_CONTINUE_PROCESSING_EXIT_REASON, USB_FSM_UART_MUTE);
    }
}

/*******************************************************************************
    FUNCTION: USB_EnqueueOutEpPacket

    SUMMARY:
    This function copies the received buffer and re-arms the OUT endpoint so
    the host can send something else if it needs to.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void USB_EnqueueOutEpPacket(void)
{
    uint16_t packet_size = USBFS_GetEPCount(USBFS_EP3);

    if (BRIDGE_FromHostEnqueue((const uint8_t*) usb_context.receive_endpoint_buffer, packet_size) == false)
    {
        //TODO(jules) debug print out fifo overflow
    }
    
    USB_ChangeEventFlags(0, USB_EVENT_FLAG_PENDING_OUT_EP);
    USBFS_EnableOutEP(USBFS_EP3);
}

/*******************************************************************************
    FUNCTION: USB_StartEnqueueingInEpPackets

    SUMMARY:
    This function initiates the first DMA transfer of up-to the maximum USB
    packet size from the transmit FIFO.  Subsequent transfers are initiated by
    the IN endpoint ISR until the frame is full.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void USB_StartEnqueueingInEpPackets(void)
{
    USB_ChangeEventFlags(0, USB_EVENT_FLAG_TX_FINAL_PACKET | USB_EVENT_FLAG_TX_FINAL_PACKET_COMPLETE);
    USB_EnqueueInEpPacket();
}

/*******************************************************************************
    FUNCTION: USB_EnqueueInEpPacket

    SUMMARY:
    This function initiates the DMA transfer of up-to the maximum USB packet
    size from the transmit FIFO.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void USB_EnqueueInEpPacket(void)
{
    uint16_t packet_size;

    USB_ChangeEventFlags(0, USB_EVENT_FLAG_TX_EXPECTING_RESPONSE);

    packet_size = (uint16_t) BRIDGE_ToHostDequeue((uint8_t*) usb_context.transmit_endpoint_buffer, USB_MAX_PACKET_SIZE);

    if (packet_size > 0)
    {
        if (packet_size < USB_MAX_PACKET_SIZE)
        {
            USB_ChangeEventFlags(USB_EVENT_FLAG_TX_FINAL_PACKET, 0);
        }
        else
        {
            USB_ChangeEventFlags(0, USB_EVENT_FLAG_TX_FINAL_PACKET);
        }
    }
    else if (USB_CheckEventFlags(USB_EVENT_FLAG_TX_FINAL_PACKET) == true)
    {
        /* Previous packet was less than max-length, so don't send
           a zero-length packet which is needed to terminate max-length
           packets. */
        USB_ChangeEventFlags(USB_EVENT_FLAG_TX_FINAL_PACKET_COMPLETE, 0);
    }
    else
    {
        /* Previous packet was max-length, so send a zero-length packet
           which is needed to terminate the frame. */
        USB_ChangeEventFlags(USB_EVENT_FLAG_TX_FINAL_PACKET, 0);
    }

    if (USB_CheckEventFlags(USB_EVENT_FLAG_TX_FINAL_PACKET_COMPLETE) == false)
    {
        uint8_t int_en = CyEnterCriticalSection();

        USBFS_LoadInEP(USBFS_EP2, USBFS_NULL, packet_size);
        USB_ChangeEventFlags(USB_EVENT_FLAG_TX_EXPECTING_RESPONSE, 0);

        CyExitCriticalSection(int_en);
    }
}
