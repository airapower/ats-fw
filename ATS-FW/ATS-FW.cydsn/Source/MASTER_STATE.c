/*******************************************************************************
           FILE:  MASTER_STATE.c

    DESCRIPTION:  Master state machine. Controls the high level functionality 
                  of the system.

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  12/10/19
*******************************************************************************/

#include "BRIDGE_Public.h"
#include "LIN_MASTER_Public.h"
#include "MASTER_STATE_Private.h"
#include "SECTIMER_Public.h"
#include "STATE_MACHINE_Public.h"
#include "SYSTIMER_Public.h"
#include "USB_Public.h"
#include "WATCHDOG_Public.h"

#include "debug.h"
#include "project.h"

/*******************************************************************************
    FUNCTION: MASTER_STATE_Start

    SUMMARY:
    This function inits the master state machine to its default values.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void MASTER_STATE_Start(void)
{
    STATE_MACHINE_InitStateMachine(&master_state_context.state_machine, STATE_MACHINE_ID_MASTER, MASTER_STATE_INIT_STATE);
}

/*******************************************************************************
    FUNCTION: MASTER_STATE_Service

    SUMMARY:
    This function should be called periodically by the master program loop to 
    service the master state machine.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void MASTER_STATE_Service(void)
{
    switch(master_state_context.state_machine.current_state)
    {
        case MASTER_STATE_INIT_STATE:
            MASTER_STATE_InitStateHandler();
            break;

        case MASTER_STATE_ERROR_STATE:
            MASTER_STATE_ErrorStateHandler();
            break;

        default:
            STATE_MACHINE_ChangeState(&master_state_context.state_machine, MASTER_STATE_INIT_STATE, MASTER_STATE_INVALID_STATE_EXIT_REASON, false);
            break;
    }

    MASTER_STATE_LoopTimeService();

    // Service APIs that are run for all modules go here
    WATCHDOG_Service();
    SYSTIMER_Service();
    USB_Service();
    BRIDGE_Service();
    SECTIMER_Service();
    LIN_MASTER_Service();
}

/*******************************************************************************
    FUNCTION: MASTER_STATE_GetLoopTimeData

    SUMMARY:
    This API is used to get a pointer to the master loop timing data struct.

    PARAMETERS:
    none

    RETURNS:
    a pointer to the current master loop timing data struct
*******************************************************************************/
const master_loop_timing_data_t* MASTER_STATE_GetLoopTimeData(void)
{
    return &master_loop_context.timing_data;
}

/*******************************************************************************
    FUNCTION: MASTER_STATE_ResetLoopTimeData

    SUMMARY:
    This function resets the loop timing data.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void MASTER_STATE_ResetLoopTimeData(void)
{
    master_loop_context.timing_data.max_loop_time_us = 0;
    master_loop_context.timing_data.avg_loop_time_us = 0;
}

/*******************************************************************************
    FUNCTION: MASTER_STATE_SetState

    SUMMARY:
    This API is used to manually set the master state machine to a particular
    state. This is used for debugging with the USB interface. One may want to
    set a specific state for testing.

    PARAMETERS:
    state - the new state to change to

    RETURNS:
    none
*******************************************************************************/
void MASTER_STATE_SetState(master_state_t state)
{
    STATE_MACHINE_ChangeState(&master_state_context.state_machine, state, MASTER_STATE_API_SET_STATE_EXIT_REASON, false);
}

/*******************************************************************************
    FUNCTION: MASTER_STATE_GetStateMachine

    SUMMARY:
    This API is used to get the master state machine.

    PARAMETERS:
    none

    RETURNS:
    address of the master state machine
*******************************************************************************/
const state_machine_t* MASTER_STATE_GetStateMachine(void)
{
    return &master_state_context.state_machine;
}

/*******************************************************************************
    FUNCTION: MASTER_STATE_InitStateHandler

    SUMMARY:
    This is the handler function for the initialization state. All startup
    functionality is performed here before proceeding to normal operation.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void MASTER_STATE_InitStateHandler(void)
{
    if(master_state_context.state_machine.first_time_in_state == FIRST_TIME_IN_STATE)
    {
        master_state_context.state_machine.first_time_in_state = !FIRST_TIME_IN_STATE;

        CyGlobalIntEnable; // Enable global interrupts

        CyDmaEnable(); // Enable the DMA controller

        usb_enumerate_context.state = USB_ENUMERATE_STATE_INIT;

        if(!master_state_context.booted)
        {
            //Start all modules 
            master_state_context.booted = true;
            SYSTIMER_Start();
            SECTIMER_Start();
            USB_Start();
            BRIDGE_Start();
            LIN_MASTER_Start();
            WATCHDOG_Start();
        }

        MASTER_STATE_SetHardwareSafeMode();
    }

    MASTER_STATE_ResetLoopTimeData();

    STATE_MACHINE_ChangeState(&master_state_context.state_machine, MASTER_STATE_RUNNING_STATE, MASTER_STATE_INIT_COMPLETE_OKAY_EXIT_REASON, false);
}

/*******************************************************************************
    FUNCTION: MASTER_STATE_ErrorStateHandler

    SUMMARY:
    This is the handler function for the error state. The error state is called 
    when a major error occurs that there is no easy recovery from.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void MASTER_STATE_ErrorStateHandler(void)
{
    if(master_state_context.state_machine.first_time_in_state == FIRST_TIME_IN_STATE)
    {
        master_state_context.state_machine.first_time_in_state = !FIRST_TIME_IN_STATE;

        //Place additional API configurations call after this comment (like ASK_enable())
        MASTER_STATE_SetHardwareSafeMode();
    }
}

/*******************************************************************************
    FUNCTION: MASTER_STATE_SetHardwareSafeMode

    SUMMARY:
    This helper function is used to put all system hardware into a known safe
    state. It's typically used when entering a new master state like the error 
    state or, manual state.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void MASTER_STATE_SetHardwareSafeMode(void)
{
    //TDB?
}

/*******************************************************************************
    FUNCTION: MASTER_STATE_USB_EnumerationCheckDone

    SUMMARY:
    This function is called by the init state if the power supply voltage is
    too low to power the pad.  This function is called repeatedly in order to
    process the USB enumeration state machine, which also includes the PDO
    renegotiation state machine as well.

    PARAMETERS:
    none

    RETURNS:
    true if the USB enumeration state machine has completed, false if it is
    still running.
*******************************************************************************/
static bool MASTER_STATE_USB_EnumerationCheckDone(void)
{
    switch (usb_enumerate_context.state)
    {
        case USB_ENUMERATE_STATE_INIT:
            usb_enumerate_context.state = USB_ENUMERATE_STATE_WAITING_ON_ENUMERATION_TIMER;
            SYSTIMER_StartTimer(&usb_enumerate_context.timer, USB_ENUMERATE_TIMEOUT_MS, SYSTIMER_UNITS_MS);
            break;

        case USB_ENUMERATE_STATE_WAITING_ON_ENUMERATION_TIMER:
            if(SYSTIMER_IsExpired(&usb_enumerate_context.timer) == true)
            {
                usb_enumerate_context.state = USB_ENUMERATE_STATE_DONE;
            }
            break;

        case USB_ENUMERATE_STATE_DONE:
        default:
            return true;
            break;
    }

    return false;
}

/*******************************************************************************
    FUNCTION: MASTER_STATE_LoopTimeService

    SUMMARY:
    This utility fuction is used to track and monitor the master loop timing.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void MASTER_STATE_LoopTimeService(void)
{
    #ifdef LOOP_TIME_ENABLE 
        //collect the loop timing data
        uint32_t loop_time_us = SYSTIMER_GetElapsed_xS(&master_loop_context.timer);
        if(loop_time_us > master_loop_context.timing_data.max_loop_time_us)
        {
            master_loop_context.timing_data.max_loop_time_us = (uint16_t)loop_time_us;
        }

        if(master_loop_context.loop_avg_count == LOOP_TIME_AVG_COUNT)
        {
            master_loop_context.timing_data.avg_loop_time_us = (uint16_t)(master_loop_context.loop_avg_us / LOOP_TIME_AVG_COUNT);
            master_loop_context.loop_avg_count = 0;
            master_loop_context.loop_avg_us = master_loop_context.timing_data.avg_loop_time_us;

            #ifdef LOOP_TIME_DEBUG_UART
                //100 is used below so that the real_time_percentage comes out in units of % out of 100
                uint8_t real_time_percent = (uint8_t)((master_loop_context.timing_data.avg_loop_time_us * CENT) / LOOP_TIME_TARGET_US);
                debug(ANSI_CYAN"LOOP TIME(uS)MAX[%d]AVG[%d](%d%%)\r\n"ANSI_RESET,master_loop_context.timing_data.max_loop_time_us,
                                                                                  master_loop_context.timing_data.avg_loop_time_us,
                                                                                  real_time_percent);
            #endif
        }
        else
        {
            master_loop_context.loop_avg_count++;
            master_loop_context.loop_avg_us += loop_time_us;
        }

        SYSTIMER_StartTimer(&master_loop_context.timer, SYSTIMER_MAX_PERIOD_US, SYSTIMER_UNITS_US);
    #endif
}

/*******************************************************************************
    FUNCTION: MASTER_STATE_CheckForSystemFaults

    SUMMARY:
    This function is called from both automatic and manual modes to verify there
    are no critical faults.  It checks Vin and system current against threshold
    values.  It also monitors the running time and performs a graceful system
    reset if a threshold is exceeded (this is to avoid effects of various
    variable roll-overs in timers and counters).

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
static void MASTER_STATE_CheckForSystemFaults(void)
{
    if(SYSTIMER_IsExpired(&master_state_context.fault_timer))
    {
        if (SYSTIMER_GetSysTime_mS() > TIME_FOR_SYSTEM_TO_RESTART_MS)
        {
            CySoftwareReset();
        }
        
        SYSTIMER_StartTimer(&master_state_context.fault_timer, FAULT_CHECK_INTERVAL_MS, SYSTIMER_UNITS_MS);
    }
}
