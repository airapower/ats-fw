/*******************************************************************************
           FILE:  WATCHDOG_Public.h

    DESCRIPTION:  APIs for controlling and servicing the watchdog timer

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  12/11/19
*******************************************************************************/
#ifndef WATCHDOG_PUBLIC_H
#define WATCHDOG_PUBLIC_H

//Public Prototypes

void WATCHDOG_Start(void);
void WATCHDOG_Service(void);
void WATCHDOG_Stop(void);

#endif //WATCHDOG_PUBLIC_H
