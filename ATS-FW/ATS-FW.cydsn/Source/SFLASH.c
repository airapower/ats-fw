/*******************************************************************************
           FILE:  SFLASH.c

    DESCRIPTION:  Supervisory Flash API. 

         AUTHOR:  David Russell
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  12/2/19
*******************************************************************************/
#include "SFLASH_Private.h"
#include "SFLASH_Public.h"
#include "project.h"
#include "version.h"

/*******************************************************************************
    FUNCTION: SFLASH_ReadMfgInfo

    SUMMARY:
    This function returns the current manufacturing information stored in the
    SFlash user configurable area if the CRC is valid.  In the event the CRC
    is not valid an empty structure is returned.

    PARAMETERS:
    mfg_info - address of the structure used to hold the returned data

    RETURNS:
    true if the read was successful, false otherwise
*******************************************************************************/
bool SFLASH_ReadMfgInfo(sflash_mfg_info_t *mfg_info)
{
    row_info_t row_info;
    bool success = SFLASH_ReadInfoRow(&row_info);

    if ((success == true) && (row_info.mfg_info_valid == true))
    {
        memcpy(mfg_info, &row_info.mfg_info, sizeof(sflash_mfg_info_t));
    }
    else
    {
        memset(mfg_info, 0, sizeof(sflash_mfg_info_t));
        success = false;
    }

    return success;
}

/*******************************************************************************
    FUNCTION: SFLASH_WriteMfgInfo

    SUMMARY:
    This function stores the provided manufacturing information into the
    SFlash user configurable area.

    PARAMETERS:
    mfg_info - address of the structure holding the data to be stored

    RETURNS:
    true if the write was successful, false otherwise
*******************************************************************************/
bool SFLASH_WriteMfgInfo(const sflash_mfg_info_t *mfg_info)
{
    row_info_t row_info;

    SFLASH_ReadInfoRow(&row_info);

    memcpy(&row_info.mfg_info, mfg_info, sizeof(sflash_mfg_info_t));
    row_info.mfg_info_valid = true;

    return SFLASH_WriteInfoRow(&row_info);
}

/*******************************************************************************
    FUNCTION: SFLASH_ReadUnitInfo

    SUMMARY:
    This function returns the current unit information stored in the
    SFlash user configurable area if the CRC is valid.  In the event the CRC
    is not valid the SFlash-specific part of the structure is cleared.

    PARAMETERS:
    unit_info - address of the structure used to hold the returned data

    RETURNS:
    true if the read was successful, false otherwise
*******************************************************************************/
bool SFLASH_ReadUnitInfo(sflash_unit_info_t *unit_info)
{
    sflash_unique_chip_id_t unique_chip_id;
    row_info_t row_info;
    bool success = SFLASH_ReadInfoRow(&row_info);

    if ((success == true) && (row_info.unit_info_sflash_valid == true))
    {
        memcpy(&unit_info->sflash, &row_info.unit_info_sflash, sizeof(sflash_unit_info_written_t));
    }
    else
    {
        memset(unit_info, 0, sizeof(sflash_unit_info_t));
        success = false;
    }

    memcpy(&unit_info->aira_serial_number[0], VERSION_PRODUCT_MODEL_NUMBER, VERSION_PRODUCT_MODEL_NUMBER_LENGTH);

    SFLASH_ReadUniqueChipId(&unique_chip_id);
    memcpy(&unit_info->aira_serial_number[VERSION_PRODUCT_MODEL_NUMBER_LENGTH], unique_chip_id.serial, SFLASH_BASE32_SERIAL_DIGITS);

    return success;
}

/*******************************************************************************
    FUNCTION: SFLASH_WriteUnitInfo

    SUMMARY:
    This function stores the provided unit information into the
    SFlash user configurable area.

    PARAMETERS:
    unit_info - address of the structure holding the data to be stored

    RETURNS:
    true if the write was successful, false otherwise
*******************************************************************************/
bool SFLASH_WriteUnitInfo(const sflash_unit_info_t *unit_info)
{
    row_info_t row_info;

    SFLASH_ReadInfoRow(&row_info);

    memcpy(&row_info.unit_info_sflash, &unit_info->sflash, sizeof(sflash_unit_info_written_t));
    row_info.unit_info_sflash_valid = true;

    return SFLASH_WriteInfoRow(&row_info);
}

/*******************************************************************************
    FUNCTION: SFLASH_ReadUniqueChipId

    SUMMARY:
    This function returns the 64-bit unique ID of the device. It also generates
    the base-32 serial encoding. The device silicon ID is also returned.

    PARAMETERS:
    unique_chip_id - address of the union used to hold the returned ID

    RETURNS:
    none
*******************************************************************************/
void SFLASH_ReadUniqueChipId(sflash_unique_chip_id_t *unique_chip_id)
{
    uint8_t index;
    uint8_t value;
    uint8_t i;
    uint64_t dword;

    CyGetUniqueId((uint32 *)unique_chip_id->chip_id.words);
    dword = unique_chip_id->chip_id.dword;

    for (i = 0; i < SFLASH_BASE32_SERIAL_DIGITS; i++)
    {
        index = (uint8_t)((SFLASH_BASE32_SERIAL_DIGITS - 1) - i);
        value = dword & SFLASH_BASE32_MASK;
        unique_chip_id->serial[index] = base32_table[value];
        dword >>= SFLASH_BASE32_SHIFT;
    }

    dword = unique_chip_id->chip_id.dword;

    for (i = 0; i < SFLASH_CHIP_UNIQUE_ID_BYTES; i++)
    {
        index = (uint8_t)((SFLASH_CHIP_UNIQUE_ID_BYTES - 1) - i);
        value = dword & 0x00000000000000FF;
        unique_chip_id->array[index] = value;
        dword >>= BITS_IN_BYTE;
    }

    unique_chip_id->silicon_id = SFLASH_ReadSiliconId();
}

/*******************************************************************************
    FUNCTION: SFLASH_ReadBootloaderVersion

    SUMMARY:
    This function returns the bootloader version (major.minor).

    PARAMETERS:
    major - address of the returned major version
    minor - address of the returned minor version

    RETURNS:
    none
*******************************************************************************/
void SFLASH_ReadBootloaderVersion(uint8_t *major, uint8_t *minor)
{
    *major = *SFLASH_BOOTLOADER_MAJOR_ADDRESS;
    *minor = *SFLASH_BOOTLOADER_MINOR_ADDRESS;
}

/*******************************************************************************
    FUNCTION: SFLASH_ReadInfoRow

    SUMMARY:
    This function returns the current information stored in the
    SFlash user configurable area if the CRC is valid.  In the event the CRC
    is not valid an empty structure is returned.

    PARAMETERS:
    row_info - address of the structure used to hold the returned data

    RETURNS:
    true if the read was successful, false otherwise
*******************************************************************************/
static bool SFLASH_ReadInfoRow(row_info_t *row_info)
{
    return SFLASH_ReadRow((uint8_t *)row_info, sizeof(row_info_t), SFLASH_INFO_ROW);
}

/*******************************************************************************
    FUNCTION: SFLASH_WriteInfoRow

    SUMMARY:
    This function stores the provided information into the
    SFlash user configurable area.

    PARAMETERS:
    row_info - address of the structure holding the data to be stored

    RETURNS:
    true if the write was successful, false otherwise
*******************************************************************************/
static bool SFLASH_WriteInfoRow(row_info_t *row_info)
{
    return SFLASH_WriteRow((const uint8_t *)row_info, sizeof(row_info_t), SFLASH_INFO_ROW);
}

/*******************************************************************************
    FUNCTION: SFLASH_ReadRow

    SUMMARY:
    This function reads data from a row of the SFlash user configurable area if
    the CRC is valid.  In the event the CRC is not valid null information is
    returned.

    PARAMETERS:
    data - address of memory that will hold the read row of SFlash
    size - number of bytes to be read from a row of SFlash
    row_num - the row number to be read (0 - 3)

    RETURNS:
    true if the read was successful, false otherwise
*******************************************************************************/
static bool SFLASH_ReadRow(uint8_t *data, uint32_t size, uint32_t row_num)
{
    uint8_t *row_data;
    bool valid = false;

    if (size <= SFLASH_INFO_BYTES)
    {
        if (SFLASH_CheckCRC(row_num) == true)
        {
            row_data = SFLASH_GetAddress(row_num, 0);
            memcpy(data, row_data, size);
            valid = true;
        }
    }

    if (!valid)
    {
        memset(data, 0, size);
    }

    return valid;
}

/*******************************************************************************
    FUNCTION: SFLASH_WriteRow

    SUMMARY:
    This function writes data to a row of the SFlash user configurable area.
    Each row is composed of data bytes followed by zeros padded to the end of
    the row minus the size of the row checksum.  At the time the data is
    written to the row, the checksum is calculated and appended at the end.

    PARAMETERS:
    data - address of data to be written to a row of SFlash
    size - number of bytes to be written to a row of SFlash
    row_num - the row number to be written (0 - 3)

    RETURNS:
    true if the write was successful, false otherwise
*******************************************************************************/
static bool SFLASH_WriteRow(const uint8_t *data, uint32_t size, uint32_t row_num)
{
    bool success = false;
    uint8_t row_data[CY_SFLASH_SIZEOF_USERROW] = {0x00};
    uint16_t crc = SFLASH_CRC16_INIT;

    if (size <= SFLASH_INFO_BYTES)
    {
        memcpy(row_data, data, size);
        crc = SFLASH_CalcCRC(crc, row_data, SFLASH_INFO_BYTES);
        row_data[SFLASH_CRC_OFFSET] = (uint8_t)(crc & 0x00FF);
        row_data[SFLASH_CRC_OFFSET + 1] = (uint8_t)((crc >> 8) & 0x00FF);

        if (CySysSFlashWriteUserRow(row_num, row_data) == CY_SYS_SFLASH_SUCCESS)
        {
            success = SFLASH_CheckCRC(row_num);
        }
    }

    return success;
}

/*******************************************************************************
    FUNCTION: SFLASH_CheckCRC

    SUMMARY:
    This function verifies the validity of the data present in a row of the
    SFlash user configurable area.

    PARAMETERS:
    row_num - the row number to be verified (0 - 3)

    RETURNS:
    true if the data matches the CRC calculation, false otherwise
*******************************************************************************/
static bool SFLASH_CheckCRC(uint32_t row_num)
{
    bool verified = false;
    uint16_t calculated_crc = SFLASH_CRC16_INIT;
    uint16_t read_crc;
    uint8_t *row_data;

    row_data = SFLASH_GetAddress(row_num, 0);
    calculated_crc = SFLASH_CalcCRC(calculated_crc, row_data, SFLASH_INFO_BYTES);

    read_crc = row_data[SFLASH_CRC_OFFSET + 1];
    read_crc = (uint16_t)((read_crc << 8) | row_data[SFLASH_CRC_OFFSET]);

    if (calculated_crc == read_crc)
    {
        verified = true;
    }

    return verified;
}

/*******************************************************************************
    FUNCTION: SFLASH_CalcCRC

    SUMMARY:
    This function computes the CRC-16 for the provided data buffer.

    PARAMETERS:
    crc - previous CRC value (internal state)
    data - address of data over which the CRC is calculated
    length - number of bytes in the data buffer

    RETURNS:
    updated CRC value (new state)
*******************************************************************************/
static uint16_t SFLASH_CalcCRC(uint16_t crc, const uint8_t *data, uint32_t length)
{
    uint32_t i;
    uint16_t new_crc = crc;

    for (i = 0; i < length; i++)
    {
        new_crc = (new_crc >> 8) ^ crc16_table[(new_crc ^ data[i]) & 0xFF];
    }

    return new_crc;
}

/*******************************************************************************
    FUNCTION: SFLASH_GetAddress

    SUMMARY:
    This function returns the address of the byte specified by the row number
    and offset within the SFlash user configurable area.

    PARAMETERS:
    row_num - the row number of the desired address (0 - 3)
    offset - offset within the row of the desired address

    RETURNS:
    address of the byte specified by the row and offset
*******************************************************************************/
static uint8_t *SFLASH_GetAddress(uint32_t row_num, uint32_t offset)
{
    uint32_t row_address;
    row_address = ((row_num * CY_SFLASH_SIZEOF_USERROW) + CY_SFLASH_USERBASE);
    row_address += offset;
    return (uint8_t *)row_address; 
}

/*******************************************************************************
    FUNCTION: SFLASH_ReadSiliconId

    SUMMARY:
    This function reads and returns the silicon ID of the device. The algorithm
    was pieced together using 
        CY8C4xxx, CYBLxxxx Programming Specifications
        (Document Number 002-22325 Rev. *E)

    PARAMETERS:
    none

    RETURNS:
    silicon ID of the device
*******************************************************************************/
static uint16_t SFLASH_ReadSiliconId(void)
{
    volatile uint8_t timeout = 0xFF;
    uint16_t silicon_id = 0;

    CY_FLASH_CPUSS_SYSARG_REG = (uint32) (CY_FLASH_KEY_TWO(SFLASH_API_OPCODE_GET_SILICON_ID) << CY_FLASH_PARAM_KEY_TWO_OFFSET  ) | CY_FLASH_KEY_ONE;
    CY_FLASH_CPUSS_SYSREQ_REG = CY_FLASH_CPUSS_REQ_START | SFLASH_API_OPCODE_GET_SILICON_ID;
    CY_NOP;

    while (CY_FLASH_CPUSS_SYSREQ_REG & (SFLASH_CPUSS_REQ_BIT | SFLASH_CPUSS_PRIVILEGED_BIT))
    {
        timeout--;
        if (timeout == 0)
        {
            break;
        }
    }

    if ((timeout > 0) && (CY_FLASH_API_RETURN == CYRET_SUCCESS))
    {
        silicon_id = CY_FLASH_CPUSS_SYSARG_REG & 0x0000FFFF;
    }

    return silicon_id;
}
