/*******************************************************************************
           FILE:  version.h

    DESCRIPTION:  Product version information. 

         AUTHOR:  David Russell
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  11/25/19
*******************************************************************************/
#ifndef VERSION_H
#define VERSION_H

#define VERSION_COPYRIGHT_STRING            "AIRA"
#define VERSION_COPYRIGHT_STRING_LENGTH     4

/* As defined in git repo org-docs, Excel document
   Product Serial Numbers.xls */
#define VERSION_PRODUCT_MODEL_1             "W" // Free Position Wireless Charger
#define VERSION_PRODUCT_MODEL_2             "C" // USB-C
#define VERSION_PRODUCT_MODEL_3             "B" // Gen 2
#define VERSION_PRODUCT_MODEL_4             "A" // 3 Device 12V Automotive Ref

#define VERSION_PRODUCT_MODEL_NUMBER        VERSION_PRODUCT_MODEL_1 \
                                            VERSION_PRODUCT_MODEL_2 \
                                            VERSION_PRODUCT_MODEL_3 \
                                            VERSION_PRODUCT_MODEL_4
#define VERSION_PRODUCT_MODEL_NUMBER_LENGTH 4

#define VERSION_FW_VERSION_MAJOR            0   // 8-bit
#define VERSION_FW_VERSION_MINOR            5   // 8-bit
#define VERSION_FW_VERSION_BUILD            0   // 32-bit

#define VERSION_API_VERSION_MAJOR           3   // 8-bit
#define VERSION_API_VERSION_MINOR           0   // 8-bit

#define VERSION_GIT_HASH                    "0d363c1"
#define VERSION_GIT_HASH_LENGTH             7

#endif //VERSION_H
