/*******************************************************************************
           FILE:  SECTIMER_Public.h

    DESCRIPTION:  Second timing API.

         AUTHOR:  John L. Winters
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  2/26/2020
*******************************************************************************/
#ifndef SECTIMER_PUBLIC_H
#define SECTIMER_PUBLIC_H

#include <stdint.h>
#include <stdbool.h>

//Public defines, enums, typedefs, etc.

typedef struct
{
    uint32_t start_time_sec;    //the start time in sec units
    uint32_t period_sec;        //the period of the timer in sec units
    bool running;               //true if timer is running, false if it's not
} sectimer_t;

//Public Prototypes

void SECTIMER_Start(void);
void SECTIMER_Service(void);
void SECTIMER_StopTimer(sectimer_t* timer);
bool SECTIMER_AddTime(sectimer_t* timer, uint32_t time_to_add_sec);
uint32_t SECTIMER_GetRemaining_Sec(sectimer_t* timer);
uint32_t SECTIMER_GetElapsed_Sec(sectimer_t* timer);
void SECTIMER_StartTimer(sectimer_t* timer, uint32_t period_sec);
bool SECTIMER_IsExpired(sectimer_t* timer);

#endif //SECTIMER_PUBLIC_H
