/*******************************************************************************
           FILE:  common.h

    DESCRIPTION:  Common defines used by multiple modules. 

         AUTHOR:  David Russell
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  12/27/19
*******************************************************************************/
#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>

// Math and conversion constants

/* These are the left/right shift amount to convert between different Q notations (i.e., binary-point location).
   The comment for each of the following are the value of 1 represented in Q notation in base-2
   e.g., 1 in Q1 is 2, 1 in Q2 is 4, etc. */
#define Q0                          0   // 1.000000000000000000000000000000000
#define Q1                          1   // 10.00000000000000000000000000000000
#define Q2                          2   // 100.0000000000000000000000000000000
#define Q3                          3   // 1000.000000000000000000000000000000
#define Q4                          4   // 10000.00000000000000000000000000000
#define Q5                          5   // 100000.0000000000000000000000000000
#define Q6                          6   // 1000000.000000000000000000000000000
#define Q7                          7   // 10000000.00000000000000000000000000
#define Q8                          8   // 100000000.0000000000000000000000000
#define Q9                          9   // 1000000000.000000000000000000000000
#define Q10                         10  // 10000000000.00000000000000000000000
#define Q11                         11  // 100000000000.0000000000000000000000
#define Q12                         12  // 1000000000000.000000000000000000000
#define Q15                         15  // 1000000000000000.000000000000000000
#define Q16                         16  // 10000000000000000.00000000000000000
#define Q18                         18  // 1000000000000000000.000000000000000
#define Q32                         32  // 100000000000000000000000000000000.0

/* For rounding, multiplying, and dividing Q numbers there isoften need for half terms, these are provided here 
 * to provide a common notation */
#define HALF_Q1     (1 << (Q1 - 1))     // 0.5 in Q1 notation
#define HALF_Q2     (1 << (Q2 - 1))     // 0.5 in Q2 notation
#define HALF_Q3     (1 << (Q3 - 1))     // 0.5 in Q3 notation
#define HALF_Q4     (1 << (Q4 - 1))     // 0.5 in Q4 notation
#define HALF_Q5     (1 << (Q5 - 1))     // 0.5 in Q5 notation
#define HALF_Q6     (1 << (Q6 - 1))     // 0.5 in Q6 notation
#define HALF_Q7     (1 << (Q7 - 1))     // 0.5 in Q7 notation
#define HALF_Q8     (1 << (Q8 - 1))     // 0.5 in Q8 notation
#define HALF_Q9     (1 << (Q9 - 1))     // 0.5 in Q9 notation
#define HALF_Q10    (1 << (Q10 - 1))    // 0.5 in Q10 notation
#define HALF_Q11    (1 << (Q11 - 1))    // 0.5 in Q11 notation
#define HALF_Q12    (1 << (Q12 - 1))    // 0.5 in Q12 notation
#define HALF_Q15    (1 << (Q15 - 1))    // 0.5 in Q15 notation
#define HALF_Q16    (1 << (Q16 - 1))    // 0.5 in Q16 notation
#define HALF_Q32    (1 << (Q32 - 1))    // 0.5 in Q32 notation

/* Rounding division and multiplication macros. The rounding assumes that *both a and b are positive*
 * values. The result of both the division & multiplication is in Qa format.
 * e.g:
 *      a_q5 = 13 << Q5;
 *      b_q3 = 20; // 2.5
 * 
 *      c_q5 = Q_DIVIDE(a_q5, b_q3, Q3) // <= c_q5 = 166 ~= 5.1875
 *      d_q5 = Q_MULTYPLY(a_q5, b_q3, Q3) // <= d_q5 = 1040 ~= 32.5
 */
#define Q_DIVIDE(a, b, Qb)      ((((a) << (Qb)) + ((b) >> 1)) / (b))
#define Q_MULTIPLY(a, b, Qb)    ((((a) * (b)) + (1 << ((Qb) - 1))) >> (Qb))

/* Rounds a *positive* value in Qa notation to a smaller Qb notation. *Qa > Qb must hold*.
 * e.g:
 *      a_q2 = 15; // 7Q2 = 3.75
 *      b_q1 = Q_ROUND(a_q2, Q1); // b_q1 = 8Q1 = 4.00
 */
#define Q_ROUND(a, Qa, Qb)      (((a) + (1 << ((Qa) - (Qb) - 1))) >> ((Qa) - (Qb)))

/* Powers of 10 conversion factors
 * These are *multiplicative* constants, so multiply/divide to convert between decimal representations.
 * e.g 1 in 0p01 is (1*D2), 100 0p001 in 0p01 is (100 / D1) 
 */
#define D0                          (1u)
#define D1                          (10u)
#define D2                          (100u)
#define D3                          (1000u)
#define D4                          (10000u)
#define D5                          (100000u)
#define D6                          (1000000u)

/* The following macro applies an exponential filter to a real-time time-series of samples, Qk,
   whose result is accumulated in Qn.  The magnitude by which the accumulator can change based
   upon a single input sample is determined by the choice of alpha, e.g.:
            Qn = alpha*Qn + (1-alpha)*Qk
  
        alpha = 0   =>  no filtering, i.e., Qn+1 = Qk
        alpha = 1   =>  no change, i.e., Qn+1 = Qn
        alpha = 0.5 =>  unweighted average, i.e., Qn+1 = (Qn + Qk) / 2
  
   For fixed-point processing, alpha can be restricted to the rationals, and to benefit processing
   it is best to select a denominator that is a power-of-2 (i.e., so the division is realized by
   the compiler/assembler as a right-shift).  With this change the filter is defined as:
            Qn = (n/d)*Qn + (1-n/d)*Qk
        =>  Qn = (n*Qn + (d-n)*Qk) / d
  
        (n,d) = (0,2)   =>  no filtering, i.e., Qn+1 = Qk
        (n,d) = (2,2)   =>  no change, i.e., Qn+1 = Qn
        (n,d) = (1,2)   =>  unweighted average, i.e., Qn+1 = (Qn + Qk) / 2
  
   An example that prevents the input sample from moving the average too quickly (i.e., "overdamped") includes:
  
        (n,d) = (7,8)   =>  87.5% accumulator and 12.5% sample, i.e., Qn+1 = (7Qn + Qk) / 8
 
   An example that allows the input sample to move the average quickly (i.e., "underdamped") includes:
  
        (n,d) = (1,8)   =>  12.5% accumulator and 87.5% sample, i.e., Qn+1 = (Qn + 7Qk) / 8 */
#define EXPONENTIAL_FILTER(n_acc,d,Qn,Qk,init)                  \
    if((init) && (Qn == 0))                                     \
        (Qn) = (Qk);                                            \
    else                                                        \
        (Qn) = (((n_acc)*(Qn) + ((d) - (n_acc))*(Qk)) / (d));

/* similar to the EXPONENTIAL_FILTER macro except the divisor is specified as a power of 2. Useful
   for cases where performance is crucial and the divide can be performed as a shift (compiler did not optimize
   this case automatically) */
#define EXPONENTIAL_FILTER_POW2_DIVISOR(n_acc,d_pow2,Qn,Qk,init)     \
    if((init) && (Qn == 0))                                     \
        (Qn) = (Qk);                                            \
    else                                                        \
        (Qn) = (((n_acc)*(Qn) + ((1 << (d_pow2)) - (n_acc))*(Qk)) >> (d_pow2));

#define DIFFERENCE_MAGNITUDE(a,b)   (((a)>(b))?((a)-(b)):((b)-(a)))     // macro for calculating an absolute difference
#define ABS(N)                      (((N)<0)?(-(N)):(N))                  // macro for calculating an absolute value
#define LIMIT(v,l,u)                (((v)>(u))?(u):(((v)<(l))?(l):(v)))       // macro for limiting a value between an upper- and lower-bound
#define MAX(a,b)                    (((a)>(b))?(a):(b))                 // macro for choosing a maximal value
#define MIN(a,b)                    (((a)<(b))?(a):(b))                 // macro for choosing a minimal value
#define IS_POW_2(x)                 (((x) & ((x) -1)) == 0)

#define UNUSED(p)                   ((p) = (p))                         // macro to remove warnings of unused variables

// Misc. constants

#define MS_PER_SEC                  (D3)    // convert between mS and Sec
#define MICRO_PER_MILLI             (D3)    // convert between uUnit and mUnit
#define MILLI_PER_ONE               (D3)    // convert between mUnit and Unit
        
#define BITS_IN_BYTE                (8u)    // number of bits in a byte
#define CENT                        (D2)    // for per cent, etc.
#define LSB_BIT                     (0x01)  // least significant bit mask

#define DEGREES_PER_CYCLE           (360u)  // Degrees in a full cycle

#endif // COMMON_H
