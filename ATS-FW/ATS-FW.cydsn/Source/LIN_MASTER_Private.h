/*******************************************************************************
           FILE:  LIN_MASTER_Private.h

    DESCRIPTION: LIN Master Private API

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2021, Aira Inc.
        CREATED:  06/29/21
*******************************************************************************/
#ifdef LIN_MASTER_PRIVATE_H
#error "LIN_MASTER_Private.h was incorrectly included in " __FILE__
#else
#define LIN_MASTER_PRIVATE_H
#endif

#include "LIN_MASTER_Public.h"
#include "STATE_MACHINE_Public.h"
#include "SYSTIMER_Public.h"
#include "RB_Public.h"
#include "LIN_NETWORK_Public.h"

//Private defines, enums, typedefs, etc.
#define LIN_MASTER_FSM_MUTE (false)

#define LIN_MASTER_GET_BIT(value, pos) (((value) >> (pos)) & 0x1)
#define LIN_MASTER_P0_OFFSET        ((uint8_t) 6u)      // Position for the P0 barity bit
#define LIN_MASTER_P1_OFFSET        ((uint8_t) 7u)      // Position for the P1 barity bit
#define LIN_MASTER_FRAME_ID_MASK    ((uint8_t) 0x3Fu)   // Valid frame ID bits mask

typedef enum
{
    LIN_MASTER_WAITING,
    LIN_MASTER_RECEIVING,
    LIN_MASTER_TRANSMITTING,
} lin_master_state_t;

typedef enum
{
    LIN_MASTER_ERROR_HEADER_TX_FAILURE,
    LIN_MASTER_ERROR_DATA_RX_TIMEOUT,
    LIN_MASTER_ERROR_DATA_TX_FAILURE,
    LIN_MASTER_ERROR_DATA_TX_TIMEOUT,
    LIN_MASTER_ERROR_SYNC_BYTE_FAILURE,
    LIN_MASTER_ERROR_PID_BYTE_FAILURE,
    LIN_MASTER_ERROR_CHECKSUM_FAILURE,
} lin_master_error_t;

typedef enum
{
    LIN_MASTER_TRANSPORT_IDLE,
    LIN_MASTER_TRANSPORT_REQUEST_PENDING,
    LIN_MASTER_TRANSPORT_REQUESTING,
    LIN_MASTER_TRANSPORT_RESPONDING,
} lin_master_transport_state_t;  

typedef enum
{
    LIN_MASTER_REASON_PUBLISH_FRAME_BEGIN,          // LIN MASTER started sending a publish frame header
    LIN_MASTER_REASON_PUBLISH_FRAME_TRANSMITTED,    // LIN MASTER finished sending a publish frame
    LIN_MASTER_REASON_SUBSCRIBE_FRAME_BEGIN,        // LIN MASTER started sending a subscribe frame header
    LIN_MASTER_REASON_SUBSCRIBE_FRAME_RECEIVED,     // LIN MASTER finished receiving a subscribe frame
    LIN_MASTER_REASON_CHECKSUM_ERROR,               // LIN MASTER received a frame with an invalid checksum
    LIN_MASTER_REASON_SYNC_BYTE_ERROR,              // LIN MASTER received a frame with an invalid sync byte
    LIN_MASTER_REASON_PID_BYTE_ERROR,               // LIN MASTER received a frame with an unexpected PID
    LIN_MASTER_REASON_SLAVE_RESPONSE_TIMEOUT_ERROR, // LIN MASTER timed out receiving a slave response
    LIN_MASTER_REASON_MASTER_REQUEST_TIMEOUT_ERROR, // LIN MASTER timed out transmittig a master resquest 
    LIN_MASTER_REASON_FRAME_HEADER_TRANSMIT_ERROR,  // LIN MASTER encountered an error transmitting a header
    LIN_MASTER_REASON_FRAME_DATA_TRANSMIT_ERROR,    // LIN MASTER encountered an error transmitting frame data
} lin_master_reason_t;

typedef struct
{
    const uint8_t num_entries;      // Number of entries in the schedule
    uint8_t current_entry;          // Current entry in use
    lin_schedule_entry_t * entries; // Array of entries in the schedule
} lin_master_schedule_t;

typedef struct
{
    bool completed;                                         // Flag indicating either completed transmission or reception
    uint8_t node_address;                                   // Node address of the messaged slave
    uint8_t frame_count;                                    // Used for slave response frame counting
    uint16_t length;                                        // total length of the message
    uint16_t index;                                         // index into the buffer for chunk handling
    uint8_t data[LIN_MASTER_TRANSPORT_MESSAGE_MAX_SIZE];    // transport layer message buffer
} lin_master_message_t;

typedef struct
{
    state_machine_t state;                          // State for LIN MASTER FSM
    lin_master_transport_state_t transport_state;   // Sub-State for the transport layer
    lin_master_schedule_t *schedule;                // Current schedule
    systimer_t timer;                               // General purpose timer for LIN MASTER

    struct
    {
        uint8_t length;                             // Number of valid bytes in frame_buf
        union
        {
            struct
            {
                uint8_t header[LIN_HEADER_SIZE];                        // Shortcut for header bytes
                uint8_t data[LIN_MAX_FRAME_SIZE];                       // Shortcut for data bytes
            };
            uint8_t full_data[LIN_HEADER_SIZE + LIN_MAX_FRAME_SIZE];    // Buffer for received bytes
        };
        
    } frame_buf;                        // Buffer for frame reception

    lin_master_message_t request;       // Buffer for the transport layer request
    lin_master_message_t response;      // Buffer for the transport layer response

} lin_master_t;

//Private Variables
static lin_master_t master;
static lin_master_schedule_t master_request_schedule = {.num_entries = 1, .entries = &master_request_entry};
static lin_master_schedule_t slave_response_schedule = {.num_entries = 1, .entries = &slave_response_entry};
static lin_master_schedule_t apache_schedule = {.num_entries = LIN_APACHE_NUM_ENTRIES, .entries = apache_entries};

//Private Prototypes
static void LIN_MASTER_GoToNextEntry(void);
static lin_schedule_entry_t *LIN_MASTER_GetCurrentEntry(void);

static void LIN_MASTER_OnAfterFrameReceive(lin_slot_t *slot);
static void LIN_MASTER_OnBeforeFrameTransmit(lin_slot_t *slot);

static bool LIN_MASTER_PackFrame(uint8_t *frame_data, lin_master_message_t *message);
static bool LIN_MASTER_UnPackFrame(uint8_t *frame_data, lin_master_message_t *message);

static void LIN_MASTER_ErrorHandler(lin_master_error_t error);
