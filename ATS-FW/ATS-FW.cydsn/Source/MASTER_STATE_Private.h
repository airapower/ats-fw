/*******************************************************************************
           FILE:  MASTER_STATE_Private.h

    DESCRIPTION:  Master state machine. Controls the high level functionality 
                  of the system.

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  12/10/19
*******************************************************************************/
#ifdef MASTER_STATE_PRIVATE_H
#error "MASTER_STATE_Private.h was incorrectly included in " __FILE__
#else
#define MASTER_STATE_PRIVATE_H
#endif

#include <stdint.h>
#include <stdbool.h>

#include "MASTER_STATE_Public.h"
#include "SECTIMER_Public.h"
#include "STATE_MACHINE_Public.h"
#include "SYSTIMER_Public.h"

#include "settings.h"

//Private defines, enums, typedefs, etc.

#define TIME_FOR_SYSTEM_TO_RESTART_MS   ((uint32_t) (0xd1457734L))                      // The system will restart after 0xd1457734 milliseconds (40 days)

#define FORCE_MANUAL_MODE               //if defined, the software will go into manual mode on boot rather than automatic
#define LOOP_TIME_ENABLE                //if defined, loop timing functionality will be enabled
//#define LOOP_TIME_DEBUG_UART           //if defined, the avg and max master loop time will be printed to the debug UART
#define LOOP_TIME_TARGET_US     1000    //The the target time for the master loop (the goal is to be <= LOOP_TIME_TARGET_MS)
#define LOOP_TIME_AVG_COUNT     1000    //The number of cycles to average when calculating the loop avg time

//#define USB_5V_BOOTLOADER_TRIGGER               //If this is defined, the system will enter bootloader when Vin < BOOTLOADER_VBUS_THRESHOLD_MV_Q8

#define FAULT_CHECK_INTERVAL_MS         (uint32_t)(100)                                 //This is the time interval that we will check for UVLO, overcurrent, and temperature
#define UVLO_THRESHOLD_OFF_MV_Q2        (uint32_t)(UVLO_THRESHOLD_OFF_MV << Q2)         //The downward trending UVLO thershold (default is 12,500mV)
#define UVLO_THRESHOLD_ON_MV_Q2         (uint32_t)(UVLO_THRESHOLD_ON_MV << Q2)          //The downward trending UVLO thershold (default is 14,500mV)
#define BOOTLOADER_VBUS_THRESHOLD_MV_Q2 (uint32_t)(BOOTLOADER_VBUS_THRESHOLD_MV << Q2)  // < this value will trigger the bootloader mode
#define MAX_IDLE_SYS_CURRENT_MA_Q2      (uint16_t)(MAX_IDLE_SYS_CURRENT_MA << Q2)       //This is the max system current with no tank drivers running (Should be 0mA) (default os 50mA)
#define OVER_CURRENT_TIMEOUT_MS         (uint32_t)(500)                                 //This is the amount of time we will wait after the over current condition, before returning to the prior state
#define USB_ENUMERATE_TIMEOUT_MS        (uint32_t)(2000)                                //This is the amount of time we will wait to see if the USB will get configured by a host machine

typedef struct
{
    state_machine_t state_machine;
    bool booted;
    systimer_t oc_timer;
    bool oc_timer_started;
    systimer_t fault_timer;
    uint32_t sys_vin_mv_q2;
    uint32_t sys_current_ma_q2;

} master_state_context_t;

typedef enum
{
    USB_ENUMERATE_STATE_INIT                            = 0,
    USB_ENUMERATE_STATE_WAITING_ON_ENUMERATION_TIMER    = 1,
    USB_ENUMERATE_STATE_DONE                            = 2,
} usb_enumerate_state_t;

typedef struct
{
    usb_enumerate_state_t state;
    systimer_t timer;
} usb_enumerate_context_t;

typedef struct
{
    master_loop_timing_data_t timing_data;
    systimer_t timer;
    uint32_t loop_avg_us;
    uint32_t loop_avg_count;
} master_loop_context_t;

//Private Variables

static master_state_context_t master_state_context;
static usb_enumerate_context_t usb_enumerate_context;
static master_loop_context_t master_loop_context;

//Private Prototypes

static void MASTER_STATE_InitStateHandler(void);
static void MASTER_STATE_ErrorStateHandler(void);

static void MASTER_STATE_SetHardwareSafeMode(void);
static bool MASTER_STATE_USB_EnumerationCheckDone(void);
static void MASTER_STATE_LoopTimeService(void);
static void MASTER_STATE_CheckForSystemFaults(void);
