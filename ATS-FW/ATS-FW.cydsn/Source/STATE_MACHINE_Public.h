/*******************************************************************************
           FILE:  STATE_MACHINE_Public.h

    DESCRIPTION:  State machine handler module

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  12/10/19
*******************************************************************************/
#ifndef STATE_MACHINE_PUBLIC_H
#define STATE_MACHINE_PUBLIC_H

#include <stdint.h>
#include <stdbool.h>

//Public defines, enums, typedefs, etc.

#define FIRST_TIME_IN_STATE     1

typedef enum
{
    STATE_MACHINE_ID_INVALID1           = 0x00,

    STATE_MACHINE_ID_MASTER             = 0x01,
    STATE_MACHINE_ID_USB                = 0x02,
    STATE_MACHINE_ID_LIN_MASTER         = 0x03,
    STATE_MACHINE_ID_BRIDGE             = 0x04,

    //add other state machine ID's here
    STATE_MACHINE_ID_INVALID2       = 0xFF,
} state_machine_ids_t;

typedef uint8_t state_machine_id_t;

typedef struct
{
    state_machine_id_t state_machine_id;
    uint8_t current_state;
    uint8_t prev_state;
    uint8_t last_state_exit_reason;
    uint8_t first_time_in_state;
} state_machine_t;

//Public Prototypes

void STATE_MACHINE_InitStateMachine(state_machine_t* state_machine_object, state_machine_id_t state_machine_id, uint8_t start_state);
void STATE_MACHINE_ChangeState(state_machine_t* state_machine_object, uint8_t new_state, uint8_t reason, bool debug_mute);

#endif //STATE_MACHINE_PUBLIC_H
