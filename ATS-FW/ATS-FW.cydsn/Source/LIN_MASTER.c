/*******************************************************************************
           FILE:  LIN_MASTER.c

    DESCRIPTION:  LIN Master module

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2021, Aira Inc.
        CREATED:  06/29/21
*******************************************************************************/
#include <stddef.h>
#include <string.h>

#include "LIN_HAL_Public.h"
#include "LIN_MASTER_Private.h"

#include "debug.h"

/*******************************************************************************
    FUNCTION: LIN_MASTER_Start

    SUMMARY:
    Starts the LIN MASTER module with the default schedule & an idle transport
    layer.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void LIN_MASTER_Start(void)
{
    //Start up the HW
    LIN_HAL_Init();

    // Start with no pending diagnostic requests
    master.transport_state = LIN_MASTER_TRANSPORT_IDLE;

    // Ensure we start off with the first entry of the normal schedule
    master.schedule = &apache_schedule;
    master.schedule->current_entry = master.schedule->num_entries;

    STATE_MACHINE_InitStateMachine(&master.state, STATE_MACHINE_ID_LIN_MASTER, LIN_MASTER_WAITING);
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_Service

    SUMMARY:
    Services the LIN MASTER FSM.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void LIN_MASTER_Service(void)
{
    lin_schedule_entry_t *entry;

    if (master.state.current_state == LIN_MASTER_WAITING)
    {
        // Start of slot
        if (master.state.first_time_in_state)
        {
            master.state.first_time_in_state = false;
            LIN_MASTER_GoToNextEntry();
            SYSTIMER_StartTimer(&master.timer, SYSTIMER_MAX_PERIOD_MS, SYSTIMER_UNITS_MS);
        }

        entry = LIN_MASTER_GetCurrentEntry();

        if (SYSTIMER_GetElapsed_xS(&master.timer) > entry->offset_ms)
        {
            if (LIN_HAL_SendFrameHeader(&entry->slot))
            {
                if (entry->slot.direction == LIN_SLOT_PUBLISH)
                {
                    STATE_MACHINE_ChangeState(&master.state, LIN_MASTER_TRANSMITTING, LIN_MASTER_REASON_PUBLISH_FRAME_BEGIN, LIN_MASTER_FSM_MUTE);
                }
                else
                {
                    // This is a SUBSCRIBE slot, so enable RX and reset frame buffer
                    LIN_HAL_SetEnableReceiving(true);
                    master.frame_buf.length = 0;
                    STATE_MACHINE_ChangeState(&master.state, LIN_MASTER_RECEIVING, LIN_MASTER_REASON_SUBSCRIBE_FRAME_BEGIN, LIN_MASTER_FSM_MUTE);
                }

                SYSTIMER_StartTimer(&master.timer, SYSTIMER_MAX_PERIOD_MS, SYSTIMER_UNITS_MS);
            }
            else
            {
                LIN_MASTER_ErrorHandler(LIN_MASTER_ERROR_HEADER_TX_FAILURE);
                STATE_MACHINE_ChangeState(&master.state, LIN_MASTER_WAITING, LIN_MASTER_REASON_FRAME_HEADER_TRANSMIT_ERROR, LIN_MASTER_FSM_MUTE);
            }
        }
    }
    else
    {
        entry = LIN_MASTER_GetCurrentEntry();
    }

    switch (master.state.current_state)
    {
    case LIN_MASTER_WAITING:
        LIN_HAL_SetEnableReceiving(false);
        break;

    case LIN_MASTER_RECEIVING:
        if (SYSTIMER_GetElapsed_xS(&master.timer) > entry->response_wait_ms)
        {
            // timed out waiting for frame data to be received
            LIN_MASTER_ErrorHandler(LIN_MASTER_ERROR_DATA_RX_TIMEOUT);
            STATE_MACHINE_ChangeState(&master.state, LIN_MASTER_WAITING, LIN_MASTER_REASON_SLAVE_RESPONSE_TIMEOUT_ERROR, LIN_MASTER_FSM_MUTE);
        }
        break;

    case LIN_MASTER_TRANSMITTING:
        if (master.state.first_time_in_state)
        {
            master.state.first_time_in_state = false;

            // Update slot data
            LIN_MASTER_OnBeforeFrameTransmit(&entry->slot);

            // initiate a data transmission
            if (LIN_HAL_SendFrameData(&entry->slot) == false)
            {
                // transmission error
                LIN_MASTER_ErrorHandler(LIN_MASTER_ERROR_DATA_TX_FAILURE);
                STATE_MACHINE_ChangeState(&master.state, LIN_MASTER_WAITING, LIN_MASTER_REASON_FRAME_DATA_TRANSMIT_ERROR, LIN_MASTER_FSM_MUTE);
            }
        }

        // Stay in transmit state until all bytes are sent out
        if (LIN_HAL_IsDoneSending() == true)
        {
            STATE_MACHINE_ChangeState(&master.state, LIN_MASTER_WAITING, LIN_MASTER_REASON_PUBLISH_FRAME_TRANSMITTED, LIN_MASTER_FSM_MUTE);
        }
        else if (SYSTIMER_GetElapsed_xS(&master.timer) > entry->response_wait_ms)
        {
            // timed out waiting for frame data to be transmitted
            LIN_MASTER_ErrorHandler(LIN_MASTER_ERROR_DATA_TX_TIMEOUT);
            STATE_MACHINE_ChangeState(&master.state, LIN_MASTER_WAITING, LIN_MASTER_REASON_MASTER_REQUEST_TIMEOUT_ERROR, LIN_MASTER_FSM_MUTE);
        }
        break;

    default:
        //TODO(jules) this should never happen error
        break;
    }
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_MessageStatus

    SUMMARY:
    Queries the state of the LIN transport layer. Users can only send messages
    whent the transport is idle & can only receive messages when the tranport is
    completed. Other status are informational.

    The lin_master_msg_status_t enum is closely related to the internal enum 
    lin_master_transport_state_t. the public enum has extra states that are not
    relevant to the internal state and vice-versa so there isn't a straight 1-to-1
    mapping.

    PARAMETERS:
    none

    RETURNS:
    the status of the LIN transport layer
*******************************************************************************/
lin_master_msg_status_t LIN_MASTER_MessageStatus(void)
{
    lin_master_msg_status_t status;

    switch (master.transport_state)
    {
        case LIN_MASTER_TRANSPORT_IDLE:
            if (master.request.completed && master.response.completed)
            {
                status = LIN_MASTER_COMPLETED;
            }
            else if (!master.request.completed && !master.response.completed)
            {
                status = LIN_MASTER_IDLE;
            }
            else
            {
                status = LIN_MASTER_FAILED;
            }
            break;

        case LIN_MASTER_TRANSPORT_REQUEST_PENDING:
        case LIN_MASTER_TRANSPORT_REQUESTING:
            status = LIN_MASTER_REQUESTING;
            break;

        case LIN_MASTER_TRANSPORT_RESPONDING:
            status = LIN_MASTER_RESPONDING;
            break;
            
        default:
            status = LIN_MASTER_FAILED;
            break;
    }

    return status;
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_SendMessage

    SUMMARY:
    Initiates a LIN transport message transaction. The first byte of message must
    be the SID byte. A message can only be sent if the transport layer status is 
    idle and the previous response message (if any) has been read out of the 
    response buffer via LIN_MASTER_ReceivedMessage().

    PARAMETERS:
    node_address - address of the slave to message
    message - SID + data to send to
    length - size of the message + 1 for the SID

    RETURNS:
    true if the message transaction was initiated.
*******************************************************************************/
bool LIN_MASTER_SendMessage(uint8_t node_address, uint8_t *message, uint16_t length)
{
    bool success = false;

    // Can't send a message unless the transport layer is idle and the response buffer is empty.
    if (master.transport_state == LIN_MASTER_TRANSPORT_IDLE && master.response.completed == false)
    {
        master.request.node_address = node_address;
        master.request.length = length;
        master.request.index = 0;
        master.request.completed = false;
        memcpy(master.request.data, message, length);

        master.transport_state = LIN_MASTER_TRANSPORT_REQUEST_PENDING;
        success = true;
    }

    return success;
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_ReceiveMessage

    SUMMARY:
    Reads out the slave response from the transport layer buffer. The first byte 
    of the message is the RSID byte. A message can only be received if the transport
    layer status is completed.
    
    PARAMETERS:
    message - buffer to copy the received message into
    length - size of the message + 1 for the RSID
    max_length - the maximum available size for message.

    RETURNS:
    true if the message buffer was updated with the slave response.
*******************************************************************************/
bool LIN_MASTER_ReceiveMessage(uint8_t *message, uint16_t *length, uint16_t max_length)
{
    bool success = false;

    // Can't read a message until the transport layer is idle and the response buffer is ready.
    if (master.transport_state == LIN_MASTER_TRANSPORT_IDLE && master.response.completed == true)
    {
        *length = master.response.length;
        memcpy(message, master.response.data, master.response.length <= max_length ? master.response.length : max_length);
        master.request.completed = false;
        master.response.completed = false;

        success = true;
    }

    return success;
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_HandleReceivedByte

    SUMMARY:
    Feeds bytes into the LIN MASTER. This function is called from the LIN HAL 
    module and should not be used by the user.
    
    PARAMETERS:
    byte - received byte from the LIN HW

    RETURNS:
    true if the LIN MASTER expects more bytes, false if the current frame is done.
*******************************************************************************/
bool LIN_MASTER_HandleReceivedByte(uint8_t byte)
{
    bool pending = false;

    if (master.state.current_state == LIN_MASTER_RECEIVING)
    {
        lin_schedule_entry_t *entry = LIN_MASTER_GetCurrentEntry();

        if (master.frame_buf.length == LIN_HEADER_SYNC_POS && byte != LIN_SYNC_FIELD_VALUE)
        {
            LIN_MASTER_ErrorHandler(LIN_MASTER_ERROR_SYNC_BYTE_FAILURE);
            STATE_MACHINE_ChangeState(&master.state, LIN_MASTER_WAITING, LIN_MASTER_REASON_SYNC_BYTE_ERROR, LIN_MASTER_FSM_MUTE);
            return pending;
        }
        else if (master.frame_buf.length == LIN_HEADER_PID_POS && byte != LIN_MASTER_FID_to_PID(entry->slot.id))
        {
            LIN_MASTER_ErrorHandler(LIN_MASTER_ERROR_PID_BYTE_FAILURE);
            STATE_MACHINE_ChangeState(&master.state, LIN_MASTER_WAITING, LIN_MASTER_REASON_PID_BYTE_ERROR, LIN_MASTER_FSM_MUTE);
            return pending;
        }

        if (master.frame_buf.length < entry->slot.length + LIN_HEADER_SIZE)
        {
            master.frame_buf.full_data[master.frame_buf.length] = byte;
            master.frame_buf.length++;
            pending = true;
        }
        else
        {
            lin_master_reason_t reason;

            if (byte == LIN_MASTER_CalculateChecksum(&entry->slot, master.frame_buf.data))
            {
                memcpy(entry->slot.data, master.frame_buf.data, entry->slot.length);
                LIN_MASTER_OnAfterFrameReceive(&entry->slot);
                reason = LIN_MASTER_REASON_SUBSCRIBE_FRAME_RECEIVED;
            }
            else
            {
                LIN_MASTER_ErrorHandler(LIN_MASTER_ERROR_CHECKSUM_FAILURE);
                reason = LIN_MASTER_REASON_CHECKSUM_ERROR;
            }

            STATE_MACHINE_ChangeState(&master.state, LIN_MASTER_WAITING, reason, LIN_MASTER_FSM_MUTE);
        }
    }

    return pending;
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_CalculateChecksum

    SUMMARY:
    Calculates the appropriate checksum for the given LIN slot & data. There are
    two checksums types in LIN 2.2A, classic and enhanced. This function handles
    both.
    
    PARAMETERS:
    slot - the LIN slot setting the parameters for the checksum calculation
    data - the bytes to checksum over.

    RETURNS:
    the correct checksum for the given slot & data.
*******************************************************************************/
uint8_t LIN_MASTER_CalculateChecksum(const lin_slot_t *slot, const uint8_t *data)
{
    uint16_t result;

    // Diagnostic frames use the `Classic` checksum while the rest use the `Enhanced` version
    if ((slot->id == LIN_MASTER_REQUEST_FRAME_ID) || (slot->id == LIN_SLAVE_RESPONSE_FRAME_ID))
    {
        result = 0;
    }
    else
    {
        result = LIN_MASTER_FID_to_PID(slot->id);
    }

    for (uint8_t i = 0; i < slot->length; i++)
    {
        result = (uint16_t)(result + data[i]);
        if (result > 0xFF)
        {
            result = (uint16_t)(result - 0xFF);
        }
    }

    return (uint8_t)~result;
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_FID_to_PID

    SUMMARY:
    Converts from a frame ID (FID) to a protected ID (PID). A PID is a FID with
    two additional parity bits.
    
    PARAMETERS:
    frame_id - the frame id to convert

    RETURNS:
    the PID corresponding to the given FID.
*******************************************************************************/
uint8_t LIN_MASTER_FID_to_PID(uint8_t frame_id)
{
    uint8_t p0 = LIN_MASTER_GET_BIT(frame_id, 0) ^ LIN_MASTER_GET_BIT(frame_id, 1) ^ LIN_MASTER_GET_BIT(frame_id, 2) ^ LIN_MASTER_GET_BIT(frame_id, 4);
    uint8_t p1 = LIN_MASTER_GET_BIT(frame_id, 1) ^ LIN_MASTER_GET_BIT(frame_id, 3) ^ LIN_MASTER_GET_BIT(frame_id, 4) ^ LIN_MASTER_GET_BIT(frame_id, 5) ^ 0x1;
    return (uint8_t)((p0 << LIN_MASTER_P0_OFFSET) | (p1 << LIN_MASTER_P1_OFFSET) | (frame_id & LIN_MASTER_FRAME_ID_MASK));
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_GoToNextEntry

    SUMMARY:
    Increments the schedule entry and optionally swaps schedules between the 
    normal schedule and the diagnostic only schedules at the correct point in
    the schedule.
    
    PARAMETERS:
    none

    RETURNS:
    none.
*******************************************************************************/
static void LIN_MASTER_GoToNextEntry(void)
{
    // Check if start of new schedule
    if (master.schedule->current_entry >= master.schedule->num_entries - 1)
    {
        switch (master.transport_state)
        {
        case LIN_MASTER_TRANSPORT_IDLE:
            // no pending diagnostic transaction so repeat normal schedule
            break;

        case LIN_MASTER_TRANSPORT_REQUEST_PENDING:
            // Pending diagnostic transaction
            master.schedule = &master_request_schedule;
            master.transport_state = LIN_MASTER_TRANSPORT_REQUESTING;
            break;

        case LIN_MASTER_TRANSPORT_REQUESTING:
            if (master.request.completed)
            {
                // Just finished diagnostic request so now we make space for the response
                master.schedule = &slave_response_schedule;
                master.transport_state = LIN_MASTER_TRANSPORT_RESPONDING;
            }
            break;

        case LIN_MASTER_TRANSPORT_RESPONDING:
            if (master.response.completed)
            {
                // Just finished diagnostic response so now we fall back to normal schedule
                master.schedule = &apache_schedule;
                master.transport_state = LIN_MASTER_TRANSPORT_IDLE;
            }
            break;

        default:
            //TODO(jules) should never happen, logic error
            break;
        }

        // Reset to beginning of entries
        master.schedule->current_entry = 0;
    }
    else
    {
        master.schedule->current_entry++;
    }
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_GetCurrentEntry

    SUMMARY:
    Shorthand for grabbing the current schedule entry.
    
    PARAMETERS:
    none

    RETURNS:
    pointer to the current schedule entry.
*******************************************************************************/
static lin_schedule_entry_t *LIN_MASTER_GetCurrentEntry(void)
{
    return &master.schedule->entries[master.schedule->current_entry];
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_OnAfterFrameReceive

    SUMMARY:
    Called after a frame reception. Dispatches frame reception notifications to
    handlers.

    PARAMETERS:
    slot - pointer to the schedule slot just received.

    RETURNS:
    none
*******************************************************************************/
static void LIN_MASTER_OnAfterFrameReceive(lin_slot_t *slot)
{
    switch (slot->id)
    {
    case LIN_SLAVE_RESPONSE_FRAME_ID:
        // just received a slave response frame
        if (LIN_MASTER_UnPackFrame(slot->data, &master.response) == false)
        {
            // TODO(jules) error handling ??
            debug(ANSI_RED "Unpacking error\r\n");
        }
        break;

    default:
        // Noop for this frame ID
        break;
    }
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_OnBeforeFrameTransmit

    SUMMARY:
    Called before a frame transmission. Dispatches frame transmission 
    notifications to handlers.

    PARAMETERS:
    slot - pointer to the schedule slot about to be transmited.

    RETURNS:
    none
*******************************************************************************/
static void LIN_MASTER_OnBeforeFrameTransmit(lin_slot_t *slot)
{
    switch (slot->id)
    {
    case LIN_MASTER_REQUEST_FRAME_ID:
        // about to transmit a master request frame
        if (LIN_MASTER_PackFrame(slot->data, &master.request) == false)
        {
            // TODO(jules) error check ??
            debug(ANSI_RED "Packing error\r\n");
        }
        break;

    case LIN_APACHE_COMMAND_FRAME_ID:
        // TODO(jules) remove this placeholder
        for (uint32_t i = 0; i < slot->length; i++)
        {
            slot->data[i]++;
        }
        break;

    default:
        // Noop for this frame ID
        break;
    }
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_PackFrame

    SUMMARY:
    Pulls data out of `message` and forms a diagnostic frame.
    Updates message internal state to keep track of multi frame messages.

    PARAMETERS:
    frame_data - frame to populate
    message - message to chunk

    RETURNS:
    true on sucess.
*******************************************************************************/
static bool LIN_MASTER_PackFrame(uint8_t *frame_data, lin_master_message_t *message)
{
    if (message->length < 1 || message->completed)
    {
        return false;
    }

    lin_pdu_t *pdu = (lin_pdu_t *)frame_data;

    pdu->node_address = message->node_address;

    if (message->index == 0 && message->length <= sizeof(pdu->single_frame.data))
    {
        pdu->protocol_control_info = LIN_SINGLE_FRAME_PCI | (message->length & LIN_PCI_DATA_MASK);

        // first byte of all messages is reserved for either the SID or RSID
        pdu->single_frame.service_id = message->data[0];
        memcpy(pdu->single_frame.data, &message->data[1], message->length);

        // Fill unused bytes with 0xFF
        memset(&pdu->single_frame.data[message->length], 0xFF, sizeof(pdu->single_frame.data) - message->length);
        message->completed = true;
    }
    else if (message->index == 0)
    {
        pdu->protocol_control_info = (uint8_t)(LIN_FIRST_FRAME_PCI | ((message->length / 256) & LIN_PCI_DATA_MASK));
        pdu->first_frame.length = (uint8_t)(message->length & 0xFF);

        // first byte of all messages is reserved for either the SID or RSID
        pdu->first_frame.service_id = message->data[0];
        memcpy(pdu->first_frame.data, &message->data[1], sizeof(pdu->first_frame.data));

        message->frame_count = 0;
        message->index = sizeof(pdu->first_frame.data) + 1; // data bytes + SID or RSID byte
    }
    else
    {
        pdu->protocol_control_info = (uint8_t)(LIN_CONSECUTIVE_FRAME_PCI | (message->frame_count & LIN_PCI_DATA_MASK));

        uint8_t *pos = &message->data[message->index];
        uint16_t remaining = message->length - message->index;

        if (remaining > sizeof(pdu->consecutive_frame.data))
        {
            memcpy(pdu->consecutive_frame.data, pos, sizeof(pdu->consecutive_frame.data));

            message->frame_count = (message->frame_count + 1) & 0x0F;
            message->index = (uint16_t)(message->index + sizeof(pdu->consecutive_frame.data));
        }
        else
        {
            memcpy(pdu->consecutive_frame.data, pos, remaining);

            // Fill unused bytes with 0xFF
            memset(&pdu->consecutive_frame.data[remaining], 0xFF, sizeof(pdu->consecutive_frame.data) - remaining);

            message->completed = true;
        }
    }

    return true;
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_UnPackFrame

    SUMMARY:
    Pulls data out of diagnostic fram into a `message`.
    Updates message internal state to keep track of multi frame messages.

    PARAMETERS:
    frame_data - data to accumulate
    message - message to populate

    RETURNS:
    true on sucess.
*******************************************************************************/
static bool LIN_MASTER_UnPackFrame(uint8_t *frame_data, lin_master_message_t *message)
{
    if (message->completed == true)
    {
        // overwritting a completed response
        return false;
    }

    //TODO(jules) check for protocol state violations
    bool success = true;
    lin_pdu_t *pdu = (lin_pdu_t *)frame_data;

    switch (pdu->protocol_control_info & LIN_PCI_TYPE_MASK)
    {
    case LIN_SINGLE_FRAME_PCI:
        message->node_address = pdu->node_address; //TODO(jules) check for bad node address?
        message->length = pdu->protocol_control_info & LIN_PCI_DATA_MASK;
        memcpy(message->data, pdu->first_frame.data, message->length);
        message->completed = true;
        break;

    case LIN_FIRST_FRAME_PCI:
        message->node_address = pdu->node_address; //TODO(jules) check for bad node address?
        message->length = (uint16_t)(((pdu->protocol_control_info & LIN_PCI_DATA_MASK) << CHAR_BIT) + pdu->first_frame.length);
        memcpy(message->data, pdu->first_frame.data, sizeof(pdu->first_frame.data));
        message->index = sizeof(pdu->first_frame.data);
        message->frame_count = 1;
        break;

    case LIN_CONSECUTIVE_FRAME_PCI:
    {
        if (message->frame_count != (pdu->protocol_control_info & LIN_PCI_DATA_MASK))
        {
            //TODO(jules) handle frame count mismatch
        }

        uint8_t *pos = &message->data[message->index];
        uint16_t remaining = message->length - message->index;
        if (remaining > sizeof(pdu->consecutive_frame.data))
        {
            memcpy(pos, pdu->consecutive_frame.data, sizeof(pdu->consecutive_frame.data));
            message->frame_count = (uint8_t)((message->frame_count + 1) & 0x0Fu);
            message->index = (uint16_t)(message->index + sizeof(pdu->consecutive_frame.data));
        }
        else
        {
            memcpy(pos, pdu->consecutive_frame.data, remaining);
            message->completed = true;
        }
    }
    default:
        success = false;
        break;
    }

    return success;
}

/*******************************************************************************
    FUNCTION: LIN_MASTER_ErrorHandler

    SUMMARY:
    Centralized error handler

    PARAMETERS:
    error - error encountered by the LIN MASTER FSM.

    RETURNS:
    none.
*******************************************************************************/
static void LIN_MASTER_ErrorHandler(lin_master_error_t error)
{
    switch (error)
    {
    case LIN_MASTER_ERROR_HEADER_TX_FAILURE:
        break;
    case LIN_MASTER_ERROR_DATA_RX_TIMEOUT:
        break;
    case LIN_MASTER_ERROR_DATA_TX_FAILURE:
        break;
    case LIN_MASTER_ERROR_DATA_TX_TIMEOUT:
        break;
    case LIN_MASTER_ERROR_SYNC_BYTE_FAILURE:
        break;
    case LIN_MASTER_ERROR_PID_BYTE_FAILURE:
        break;
    case LIN_MASTER_ERROR_CHECKSUM_FAILURE:
        break;
    default:
        break;
    }
}
