/*******************************************************************************
           FILE:  SYSTIMER_Public.h

    DESCRIPTION:  System timing API.

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  11/4/19
*******************************************************************************/
#ifndef SYSTIMER_PUBLIC_H
#define SYSTIMER_PUBLIC_H

#include <stdint.h>
#include <stdbool.h>

//Public defines, enums, typedefs, etc.

typedef enum
{
    SYSTIMER_UNITS_SEC =  24000000,    //The number of timer ticks for 1Sec
    SYSTIMER_UNITS_MS  =  24000,       //The number of timer ticks for 1mS
    SYSTIMER_UNITS_US  =  24           //The number of timer ticks for 1uS
} systimer_units_t;

#define SYSTIMER_MAX_PERIOD_TICKS   0xFFFFFFFF                                                  //maximum period of the system timer in ticks
#define SYSTIMER_MAX_PERIOD_US      (uint32_t)(SYSTIMER_MAX_PERIOD_TICKS / SYSTIMER_UNITS_US)   //maximum period of a uS timer
#define SYSTIMER_MAX_PERIOD_MS      (uint32_t)(SYSTIMER_MAX_PERIOD_TICKS / SYSTIMER_UNITS_MS)   //maximum period of a mS timer
#define SYSTIMER_MAX_PERIOD_SEC     (uint32_t)(SYSTIMER_MAX_PERIOD_TICKS / SYSTIMER_UNITS_SEC)  //maximum period of a Sec timer

typedef struct
{
    uint32_t start_time_41p666nS;   //the start timestamp in system timer ticks
    uint32_t period_xs;             //the period of the timer in the units specified by "units"
    bool running;                   //true if timer is running, false is it's not
    systimer_units_t units;         //The units selected for the timing operation
} systimer_t;

//Public Prototypes

void SYSTIMER_Start(void);
void SYSTIMER_Service(void);
uint32_t SYSTIMER_GetSysTime_mS(void);
void SYSTIMER_StopTimer(systimer_t* timer);
bool SYSTIMER_AddTime(systimer_t* timer, uint32_t time_to_add_xs);
uint32_t SYSTIMER_GetRemaining_xS(systimer_t* timer);
uint32_t SYSTIMER_GetElapsed_xS(systimer_t* timer);
bool SYSTIMER_StartTimer(systimer_t* timer, uint32_t period, systimer_units_t units);
bool SYSTIMER_IsExpired(systimer_t* timer);

#endif //SYSTIMER_PUBLIC_H
