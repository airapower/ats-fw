/*******************************************************************************
           FILE:  MASTER_STATE_Public.h

    DESCRIPTION:  Master state machine. Controls the high level functionality 
                  of the system.

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  12/10/19
*******************************************************************************/
#ifndef MASTER_STATE_PUBLIC_H
#define MASTER_STATE_PUBLIC_H

#include <stdint.h>
#include <stdbool.h>
    
#include "STATE_MACHINE_Public.h"
#include "common.h"

//Public defines, enums, typedefs, etc.

typedef enum
{
    MASTER_STATE_RESERVED_STATE     = 0,
    MASTER_STATE_INIT_STATE         = 1,
    MASTER_STATE_RUNNING_STATE      = 2,
    MASTER_STATE_ERROR_STATE        = 3,
} master_state_t;

typedef enum
{
    MASTER_STATE_RESERVED_EXIT_REASON                = 0,  //Reserved so that zero is not used as a reason code
    MASTER_STATE_INVALID_STATE_EXIT_REASON           = 1,  //Somehow the state machine ended up in an invalid state
    MASTER_STATE_INIT_COMPLETE_OKAY_EXIT_REASON      = 2,  //INIT STATE - completed successfully
    MASTER_STATE_API_SET_STATE_EXIT_REASON
} master_state_exit_reason_t;

typedef struct
{
    uint16_t max_loop_time_us;
    uint16_t avg_loop_time_us; 
} master_loop_timing_data_t;

//Public Prototypes

void MASTER_STATE_Start(void);
void MASTER_STATE_Service(void);
const master_loop_timing_data_t* MASTER_STATE_GetLoopTimeData(void);
void MASTER_STATE_ResetLoopTimeData(void);
void MASTER_STATE_SetState(master_state_t state);
const state_machine_t* MASTER_STATE_GetStateMachine(void);

#endif //MASTER_STATE_PUBLIC_H
