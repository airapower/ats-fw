/*******************************************************************************
           FILE:  STATE_MACHINE.c

    DESCRIPTION:  State machine handler module

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  12/10/19
*******************************************************************************/

#include "STATE_MACHINE_Private.h"
#include "STATE_MACHINE_Public.h"
#include "debug.h"

/*******************************************************************************
    FUNCTION: STATE_MACHINE_InitStateMachine

    SUMMARY:
    This function is called to init a state object to defaults

    PARAMETERS:
    state_machine_object - the state machine object that needs updating
    state_machine_id - the id to assign to the state machine object
    start_state - the state that the state machine will start on

    RETURNS:
    none
*******************************************************************************/
void STATE_MACHINE_InitStateMachine(state_machine_t* state_machine_object, state_machine_id_t state_machine_id, uint8_t start_state)
{
    state_machine_object->state_machine_id = state_machine_id;
    state_machine_object->prev_state = 0;
    state_machine_object->current_state = start_state;
    state_machine_object->last_state_exit_reason = 0;
    state_machine_object->first_time_in_state = FIRST_TIME_IN_STATE;
}

/*******************************************************************************
    FUNCTION: STATE_MACHINE_ChangeState

    SUMMARY:
    This function is called to request a change of state

    PARAMETERS:
    state_machine_object - the state machine object that needs updating
    new_state - the new state that the state machine will be changing to
    reason - the reason that the state machine is leaving its current state
    debug_mute - set to true to prevent this state transision from being sent
                 to the debug module

    RETURNS:
    none
*******************************************************************************/
void STATE_MACHINE_ChangeState(state_machine_t* state_machine_object, uint8_t new_state, uint8_t reason, bool debug_mute)
{
    if(state_machine_object->current_state == new_state)
    {
        return; //if current state is the same as the new state, there is nothing we can do here....
    }

    if(!debug_mute)
    {
        #ifdef STATE_MACHINE_UART_DEBUG
            //This switch state is just used to pick a color based on the state ID
            switch(state_machine_object->state_machine_id)
            {
                case STATE_MACHINE_ID_MASTER:
                    debug( ANSI_RED "State Change ID[%d] OLD[%d] > NEW[%d] Reason[%d]\r\n" ANSI_RESET,
                        state_machine_object->state_machine_id,
                        state_machine_object->current_state,
                        new_state,
                        reason);
                    break;

                default:
                    debug( ANSI_GREEN "State Change ID[%d] OLD[%d] > NEW[%d] Reason[%d]\r\n" ANSI_RESET,
                        state_machine_object->state_machine_id,
                        state_machine_object->current_state,
                        new_state,
                        reason);
                    break;
            }
        #endif
    }

    state_machine_object->prev_state = state_machine_object->current_state;
    state_machine_object->current_state = new_state;
    state_machine_object->last_state_exit_reason = reason;
    state_machine_object->first_time_in_state = FIRST_TIME_IN_STATE;
}
