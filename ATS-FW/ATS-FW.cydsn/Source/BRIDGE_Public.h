/*******************************************************************************
           FILE:  BRIDGE_Public.h

    DESCRIPTION:  BRIDGE Public API

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2021, Aira Inc.
        CREATED:  07/26/2021
*******************************************************************************/
#ifndef BRIDGE_PUBLIC_H
#define BRIDGE_PUBLIC_H

#include <stdint.h>
#include <stdbool.h>
    
void BRIDGE_Start(void);
void BRIDGE_Service(void);

uint32_t BRIDGE_ToHostBytesReadyToTransmit(void);
uint32_t BRIDGE_ToHostDequeue(uint8_t *buffer, uint32_t max_len);
bool BRIDGE_FromHostEnqueue(const uint8_t *buffer, uint32_t length);

#endif // BRIDGE_PUBLIC_H
