/*******************************************************************************
           FILE:  SFLASH_Public.h

    DESCRIPTION:  Supervisory Flash API. 

         AUTHOR:  David Russell
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  12/2/19
*******************************************************************************/
#ifndef SFLASH_PUBLIC_H
#define SFLASH_PUBLIC_H

#include <stdint.h>
#include <stdbool.h>

#include "version.h"
#include "common.h"

//Public defines, enums, typedefs, etc.

#define SFLASH_MFG_ID_LENGTH                5       // Number of ASCII characters used for the Mfg ID
#define SFLASH_HW_REV_LENGTH                4       // Number of ASCII characters used for the HW revision
#define SFLASH_CHIP_UNIQUE_ID_WORDS         2       // Number of 32-bit words that make up the PSoC unique ID
#define SFLASH_CHIP_UNIQUE_ID_BYTES         8       // Number of 8-bit bytes that make up the PSoC unique ID
#define SFLASH_BASE32_SERIAL_DIGITS         13      // Number of digits used for the serial string
#define SFLASH_AIRA_SERIAL_NUMBER_LENGTH    (VERSION_PRODUCT_MODEL_NUMBER_LENGTH + SFLASH_BASE32_SERIAL_DIGITS) // Total number of digits that make up a serial number

typedef enum
{
    SFLASH_ASSEMBLY_VERSION_UNDEFINED = 0,
    SFLASH_ASSEMBLY_VERSION_1 = 1,
    SFLASH_ASSEMBLY_VERSION_2 = 2,
    SFLASH_ASSEMBLY_VERSION_3 = 3,
    SFLASH_ASSEMBLY_VERSION_MAX = 4
} sflash_assembly_version_t;

typedef struct __attribute__((packed))
{
    char id[SFLASH_MFG_ID_LENGTH];
    uint64_t time;
} sflash_mfg_info_t; 

typedef struct __attribute__((packed))
{
    char hw_revision[SFLASH_HW_REV_LENGTH];
    uint8_t assembly_version;
} sflash_unit_info_written_t;

typedef struct
{
    sflash_unit_info_written_t sflash;
    char aira_serial_number[SFLASH_AIRA_SERIAL_NUMBER_LENGTH];  // generated, not stored in SFLASH
} sflash_unit_info_t; 

typedef struct
{
    union
    {
        uint32_t words[SFLASH_CHIP_UNIQUE_ID_WORDS];
        uint64_t dword;
    } chip_id;
    uint8_t array[SFLASH_CHIP_UNIQUE_ID_BYTES];
    char serial[SFLASH_BASE32_SERIAL_DIGITS];
    uint16_t silicon_id;
} sflash_unique_chip_id_t;

//Public Prototypes

bool SFLASH_ReadMfgInfo(sflash_mfg_info_t *mfg_info);
bool SFLASH_WriteMfgInfo(const sflash_mfg_info_t *mfg_info);
bool SFLASH_ReadUnitInfo(sflash_unit_info_t *unit_info);
bool SFLASH_WriteUnitInfo(const sflash_unit_info_t *unit_info);
void SFLASH_ReadUniqueChipId(sflash_unique_chip_id_t *unique_chip_id);
void SFLASH_ReadBootloaderVersion(uint8_t *major, uint8_t *minor);

#endif //SFLASH_PUBLIC_H
