/*******************************************************************************
           FILE:  SECTIMER.c

    DESCRIPTION:  Second timing API.

         AUTHOR:  John L. Winters
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  02/26/2020
*******************************************************************************/
#include "SYSTIMER_Public.h"
#include "SECTIMER_Private.h"
#include "SECTIMER_Public.h"
#include "common.h"

/*******************************************************************************
    FUNCTION: SECTIMER_Start

    SUMMARY:
    This function initializes the SECTIMER module.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void SECTIMER_Start(void)
{
    sectimer_context.counter_sec = 0;
    sectimer_context.trigger_ms = MS_PER_SEC;
}

/*******************************************************************************
    FUNCTION: SECTIMER_Service

    SUMMARY:
    This function services and updates the SECTIMER module. 

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void SECTIMER_Service(void)
{
    uint32_t current_time_ms = SYSTIMER_GetSysTime_mS();

    if (current_time_ms > sectimer_context.trigger_ms)
    {
        sectimer_context.counter_sec++;
        sectimer_context.trigger_ms += MS_PER_SEC;
    }
}

/*******************************************************************************
    FUNCTION: SECTIMER_StopTimer

    SUMMARY:
    Stops a timer.

    PARAMETERS:
    timer - the timer object

    RETURNS:
    none
*******************************************************************************/
void SECTIMER_StopTimer(sectimer_t* timer)
{
    timer->running = false;
}

/*******************************************************************************
    FUNCTION: SECTIMER_AddTime

    SUMMARY:
    Add time to an already running timer

    PARAMETERS:
    timer - the timer object
    time_to_add_sec - the time to add in sec units

    RETURNS:
    true if time was added, false otherwise
*******************************************************************************/
bool SECTIMER_AddTime(sectimer_t* timer, uint32_t time_to_add_sec)
{
    if(SECTIMER_IsExpired(timer))
    {
        return false;
    }

    uint32_t time_remaining_sec = SECTIMER_GetRemaining_Sec(timer);
    uint64_t new_period_sec = (uint64_t)time_remaining_sec + (uint64_t)time_to_add_sec;

    if(new_period_sec > SECTIMER_MAX_PERIOD_SEC)
    {
        return false;
    }

    timer->start_time_sec = sectimer_context.counter_sec;
    timer->period_sec = (uint32_t)new_period_sec;
    timer->running = true;

    return true;
}

/*******************************************************************************
    FUNCTION: SECTIMER_GetRemaining_Sec

    SUMMARY:
    Returns the remaining time of a running timer.

    PARAMETERS:
    timer - the timer object

    RETURNS:
    the time remaining before expiring in sec units
*******************************************************************************/
uint32_t SECTIMER_GetRemaining_Sec(sectimer_t* timer)
{
    uint32_t time_remaining_sec = 0;
    uint32_t time_elapsed_sec;

    if(timer->running)
    {
        time_elapsed_sec = SECTIMER_GetElapsed_Sec(timer);
        
        //if the elapsed time has exceeded the period, we should return zero
        if(time_elapsed_sec >= timer->period_sec)
        {
            return time_remaining_sec;
        }
        
        time_remaining_sec = timer->period_sec - time_elapsed_sec;
    }

    return time_remaining_sec;
}

/*******************************************************************************
    FUNCTION: SECTIMER_GetElapsed_Sec

    SUMMARY:
    Returns the elapsed seconds since the timer was started

    PARAMETERS:
    timer - the timer object

    RETURNS:
    the time since the timer was started in sec units
*******************************************************************************/
uint32_t SECTIMER_GetElapsed_Sec(sectimer_t* timer)
{
    uint32_t time_elapsed_sec = 0;

    if(timer->running)
    {
        time_elapsed_sec = sectimer_context.counter_sec - timer->start_time_sec;

        //if the elapsed time has exceed the period, we should set the timer flag to stopped
        if(time_elapsed_sec >= timer->period_sec)
        {
            timer->running = false;
        }
    }

    return time_elapsed_sec;
}

/*******************************************************************************
    FUNCTION: SECTIMER_StartTimer

    SUMMARY:
    Starts a timer object with specified period.

    PARAMETERS:
    timer - the timer object
    period_sec - the period that the timer will run for in sec units

    RETURNS:
    none
*******************************************************************************/
void SECTIMER_StartTimer(sectimer_t* timer, uint32_t period_sec)
{
    timer->start_time_sec = sectimer_context.counter_sec;
    timer->period_sec = period_sec;
    timer->running = true;
}

/*******************************************************************************
    FUNCTION: SECTIMER_IsExpired

    SUMMARY:
    Check to see if a timer has expired (counted past its set period)

    PARAMETERS:
    timer - the timer object

    RETURNS:
    true if timer has expired, false otherwise
*******************************************************************************/
bool SECTIMER_IsExpired(sectimer_t* timer)
{
    if(SECTIMER_GetRemaining_Sec(timer) > 0)
    {
        return false;
    }

    return true;
}
