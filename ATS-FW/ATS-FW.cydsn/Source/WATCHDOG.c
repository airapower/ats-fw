/*******************************************************************************
           FILE:  WATCHDOG.c

    DESCRIPTION:  APIs for controlling and servicing the watchdog timer

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  12/11/19
*******************************************************************************/

#include "WATCHDOG_Private.h"
#include "WATCHDOG_Public.h"
#include "debug.h"
#include "project.h"

/*******************************************************************************
    FUNCTION: WATCHDOG_Start

    SUMMARY:
    This API is used to start the watchdog timer. The WDT uses the system's
    32kHz clock which by default is +/-60%. If the WDT counter is allowed to
    lapse it will reset the system.

    In addition to starting the WDT this function also checks the reason for
    the last system reset. If the last reset was somthing other than a power
    cycle or reset pin toggle event this function will log it as an error.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void WATCHDOG_Start(void)
{
    #ifdef WATCHDOG_ENABLED
        //first step is to see if we got reset by WDT1 on the last runtime
        uint32_t reset_reason = CySysGetResetReason(CY_SYS_RESET_WDT);

        if(reset_reason != 0)
        {
            debug(ANSI_YELLOW "<WDT RESET:%ld>\r\n" ANSI_RESET, reset_reason);
        }

        // Set WDT counter 0 to generate reset on match 
        CySysWdtWriteMode(CY_SYS_WDT_COUNTER0, CY_SYS_WDT_MODE_RESET);
        CySysWdtWriteMatch(CY_SYS_WDT_COUNTER0, WATCHDOG_COUNT0_MATCH);
        CySysWdtWriteClearOnMatch(CY_SYS_WDT_COUNTER0, 1u);

        // Enable WDT counters 0
        CySysWdtEnable(CY_SYS_WDT_COUNTER0_MASK);

        WATCHDOG_Service();
    #endif
}

/*******************************************************************************
    FUNCTION: WATCHDOG_Service

    SUMMARY:
    This API is used to feed the watchdog timer. It should be called
    periodically by the main loop.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void WATCHDOG_Service(void)
{
    #ifdef WATCHDOG_ENABLED
        CY_SYS_WDT_CONTROL_REG |= CY_SYS_WDT_COUNTER0_RESET;
    #endif
}

/*******************************************************************************
    FUNCTION: WATCHDOG_Stop

    SUMMARY:
    This API is used to stop the WDT from running. If this API is called the WDT
    will be disbaled until start is called again.

    PARAMETERS:
    none

    RETURNS:
    none
*******************************************************************************/
void WATCHDOG_Stop(void)
{
    #ifdef WATCHDOG_ENABLED
        CySysWdtDisable(CY_SYS_WDT_COUNTER0_MASK);
    #endif
}
