/*******************************************************************************
           FILE:  BRIDGE_Private.h

    DESCRIPTION: BRIDGE Private API

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2021, Aira Inc.
        CREATED:  07/26/21
*******************************************************************************/
#ifdef BRIDGE_PRIVATE_H
#error "BRIDGE_Private.h was incorrectly included in " __FILE__
#else
#define BRIDGE_PRIVATE_H
#endif

#include "BRIDGE_Public.h"
#include "STATE_MACHINE_Public.h"
#include "RB_Public.h"

//Private defines, enums, typedefs, etc.
#define BRIDGE_FSM_MUTE (false)

#define BRIDGE_LIN_SLAVE_NAD    (0x42u)
#define BRIDGE_LIN_COMS_SID     ((uint8_t) 0xB8u)

#define BRIDGE_WORKING_BUFFER_SIZE  (128u)
#define BRIDGE_IO_BUFFER_SIZE       (256u)

typedef enum
{
    BRIDGE_STATE_WAITING,
    BRIDGE_STATE_TRANSMITTING,
    BRIDGE_STATE_RECEIVING,
} bridge_state_t;

typedef enum
{
    BRIDGE_REASON_DEQUEUED_FROM_HOST_MESSAGE,
    BRIDGE_REASON_TRANSMITTED_TO_DUT_MESSAGE,
    BRIDGE_REASON_TRANSMITTED_TO_DUT_ERROR,
    BRIDGE_REASON_RECEIVED_FROM_DUT_MESSAGE,
    BRIDGE_REASON_RECEIVED_FROM_DUT_ERROR,
    BRIDGE_REASON_ENQUEUED_TO_HOST_ERROR
} bridge_reason_t;

typedef enum
{
    BRIDGE_ERROR_TO_DUT_TRANSMIT,
    BRIDGE_ERROR_FROM_DUT_RECEIVE,
    BRIDGE_ERROR_TO_HOST_OVERFLOW,
} bridge_error_t;

typedef struct __attribute__((packed))
{
    uint16_t cmd_id;
    uint8_t length;
} bridge_message_header_t;

typedef struct 
{
    uint8_t bytes[BRIDGE_WORKING_BUFFER_SIZE];
    uint16_t length;
} bridge_buffer_t;

typedef struct
{
    state_machine_t state;                              // Bridge state

    RB_t from_host;                                     // Incoming bytes from the host
    RB_t to_host;                                       // Outgoing bytes to the host

    bridge_buffer_t working_buffer;                     // *unwrapped* buffer for command parsing & processing

    uint8_t from_host_rb_buffer[BRIDGE_IO_BUFFER_SIZE]; // Allocated buffer backing the from_host ring buffer
    uint8_t to_host_rb_buffer[BRIDGE_IO_BUFFER_SIZE];   // Allocated buffer backing the to_host ring buffer
} bridge_context_t;


//Private Variables
static bridge_context_t bridge;

//Private Prototypes
static uint32_t BRIDGE_FromHostBytesReadyToTransmit(void);
static bool BRIDGE_ToHostEnqueue(const uint8_t *buffer, uint32_t length);
static uint32_t BRIDGE_FromHostDequeue(uint8_t *buffer, uint32_t max_length);

static void BRIDGE_ErrorHandler(bridge_error_t error);
