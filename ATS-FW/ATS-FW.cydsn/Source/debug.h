 /* ===========================================================================
           FILE:  debug.h

    DESCRIPTION:  Standard debug for serial output

         AUTHOR:  Eric Goodchild
        COMPANY:  Aira
        CREATED:  10/3/19
 ============================================================================*/
  
/*===INCLUDES================================================================*/
#include <project.h>
#include <stdio.h>

#define SERIAL_DEBUG_ENABLED

#ifndef DEBUG_H
#define DEBUG_H

#ifdef SERIAL_DEBUG_ENABLED

  char strBuffer[100];  //temp buffer used for debug string formatting         

  #define ANSI_RED     "\x1b[31m"
  #define ANSI_GREEN   "\x1b[32m"
  #define ANSI_YELLOW  "\x1b[33m"
  #define ANSI_BLUE    "\x1b[34m"
  #define ANSI_MAGENTA "\x1b[35m"
  #define ANSI_CYAN    "\x1b[36m"
  #define ANSI_WHITE   "\x1b[37m"
  #define ANSI_RESET   "\x1b[0m"
     
 
    #define debug(...) do { \
    sprintf(strBuffer, __VA_ARGS__); \
    DEBUG_UART_UartPutString(strBuffer); \
    } while(0)


#else
  #define debug(...)
#endif

#endif
/* [] END OF FILE */
