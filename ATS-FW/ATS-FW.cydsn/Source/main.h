/*******************************************************************************
           FILE:  main.h

    DESCRIPTION:  Main program loop and master state machine.

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  11/1/19
*******************************************************************************/
#ifdef MAIN_H
#error "main.h was incorrectly included in " __FILE__
#else
#define MAIN_H
#endif

//Private defines, enums, typedefs, etc.

#define STACK_TEST_PATTERN      0xBEEFu                                         // test pattern to place on the stack
#define STACK_TEST_BLOCK_SIZE   0x08u                                           // Block size to be tested. Should be EVEN
#define PSOC_SRAM_SIZE          CYDEV_SRAM_SIZE                                 // PSoC memory size
#define PSOC_SRAM_BASE          CYDEV_SRAM_BASE                                 // PSoC memory base address

#define PSOC_STACK_BASE         (CYDEV_SRAM_BASE + CYDEV_SRAM_SIZE)             // PSoC stack base address
#define PSOC_STACK_SIZE         (CYDEV_STACK_SIZE)                              // PSoC stack size
#define PSOC_STACK_END          ((uint32_t)(PSOC_STACK_BASE - PSOC_STACK_SIZE)) // PSoC stack end
//PSOC 4200L(16Kb SRAM with a 0x0800 stack)STACK_END should = 0x200003800

//Private Prototypes

static void InitStackTest(void);
