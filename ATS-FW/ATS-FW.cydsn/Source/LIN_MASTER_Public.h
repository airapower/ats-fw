/*******************************************************************************
           FILE:  LIN_MASTER_Public.h

    DESCRIPTION:  LIN Master Public API

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2021, Aira Inc.
        CREATED:  06/29/2021
*******************************************************************************/
#ifndef LIN_MASTER_PUBLIC_H
#define LIN_MASTER_PUBLIC_H

#include <stdbool.h>
#include <stdint.h>
    
#include "LIN_NETWORK_Public.h"

#define LIN_MASTER_NODE_ADDRESS (0x7D)
#define LIN_MASTER_TRANSPORT_MESSAGE_MAX_SIZE   (256u)

typedef enum
{
    LIN_MASTER_IDLE,
    LIN_MASTER_REQUESTING,
    LIN_MASTER_RESPONDING,
    LIN_MASTER_COMPLETED,
    LIN_MASTER_FAILED,
} lin_master_msg_status_t;

void LIN_MASTER_Start(void);
void LIN_MASTER_Service(void);

lin_master_msg_status_t LIN_MASTER_MessageStatus(void);
bool LIN_MASTER_SendMessage(uint8_t node_address, uint8_t *message, uint16_t length);
bool LIN_MASTER_ReceiveMessage(uint8_t *message, uint16_t *length, uint16_t max_length);

bool LIN_MASTER_HandleReceivedByte(uint8_t byte);
uint8_t LIN_MASTER_CalculateChecksum(const lin_slot_t *slot, const uint8_t *data);
uint8_t LIN_MASTER_FID_to_PID(uint8_t frame_id);

#endif // LIN_MASTER_PUBLIC_H
