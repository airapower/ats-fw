/*******************************************************************************
           FILE:  LIN_NETWORK.c

    DESCRIPTION:  LIN Network definition

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2021, Aira Inc.
        CREATED:  06/29/21
*******************************************************************************/

#include "LIN_NETWORK_Public.h"

lin_schedule_entry_t master_request_entry = {
    .offset_ms = 5,
    .response_wait_ms = 10,
    .slot = {
        .id = LIN_MASTER_REQUEST_FRAME_ID,
        .direction = LIN_SLOT_PUBLISH,
        .length = LIN_MAX_FRAME_SIZE,
    }
};

lin_schedule_entry_t slave_response_entry = {
    .offset_ms = 5,
    .response_wait_ms = 10,
    .slot = {
        .id = LIN_SLAVE_RESPONSE_FRAME_ID,
        .direction = LIN_SLOT_SUBSCRIBE,
        .length = LIN_MAX_FRAME_SIZE,
    }
};

lin_schedule_entry_t apache_entries[LIN_APACHE_NUM_ENTRIES] =
{
    {
        .offset_ms = 15,
        .response_wait_ms = 10,
        .slot = {
            .id = LIN_APACHE_CELLS_DRIVE_FRAME_ID,
            .direction = LIN_SLOT_SUBSCRIBE,
            .length = LIN_MAX_FRAME_SIZE,
        }
    },
    {
        .offset_ms = 15,
        .response_wait_ms = 10,
        .slot = {
            .id = LIN_APACHE_CELLS_STATUS_FRAME_ID,
            .direction = LIN_SLOT_SUBSCRIBE,
            .length = LIN_MAX_FRAME_SIZE,
        }
    },
    {
        .offset_ms = 15,
        .response_wait_ms = 10,
        .slot = {
            .id = LIN_APACHE_COMMAND_FRAME_ID,
            .direction = LIN_SLOT_PUBLISH,
            .length = LIN_MAX_FRAME_SIZE,
        }
    },
    {
        .offset_ms = 15,
        .response_wait_ms = 10,
        .slot = {
            .id = LIN_APACHE_CONTROL_FRAME_ID,
            .direction = LIN_SLOT_PUBLISH,
            .length = LIN_MAX_FRAME_SIZE,
        }
    }
};
