/*******************************************************************************
           FILE:  settings.h

    DESCRIPTION:  Globally used settings and limits

         AUTHOR:  David Russell
      COPYRIGHT:  Copyright 2021, Aira Inc.
        CREATED:  01/14/21
*******************************************************************************/
#ifndef SETTINGS_H
#define SETTINGS_H

//#define ENABLE_PD_CONTROLLER                                        //Define to run PD controller to negotiate power, leave undefined to disable any code that is used with the PD controller

//Absolute voltage and current numbers
#define BOOTLOADER_VBUS_THRESHOLD_MV                6000            //IF the system voltage is below this threshold, the pad assumes bootloader operation (when enabled)
#define MAX_IDLE_SYS_CURRENT_MA                     50              //On startup the power electronics must not be drawing more than this current
#define SAFE_INITIAL_PWM_FREQUENCY_HZ               (200uL * D3)    // Initial frequency of the PWM of 200kHz; selected to ensure power output is not too high
#define SAFE_INITIAL_PWM_DUTY_PER                   (10u)           // Initial duty cycle of the PWM  of 10%; selected to ensure power output is not too high
#define DEFAULT_OPERATING_FREQUENCY_HZ              (126 * D3)      //All digital pings occur at 126kHz; as of now operating frequency is fixed
#define DEFAULT_INITIAL_PWM_DUTY_0P1PER             (68)            //All digital pings occur are ramped starting at 6.8% to avoid pop rocks
#define MIN_PID_VIN_MV                              10500           //Vin minimum threshold for proper pad operation
#define CELL_VOLTAGE_LOGGING_RESET_MV_Q2            (7000 << Q2)    //If Vin had previously logged a minimum error, then it needs to rise above this threshold to clear the log restriction
#define LOGIC_3P3_MV                                3300            //3.3V definition
#define CELL_TOO_HOT_LIMIT_C_Q1                     (125 << Q1)     //temperature above which we need to transition a cell into over-temp error state
#define CELL_NOT_TOO_HOT_LIMIT_C_Q1                 (100 << Q1)     //temperature below which we can transition a cell out-of over-temp error state

#ifdef ENABLE_PD_CONTROLLER
    #define PD_POWER_SETTINGS_MV                        {12000}     //This is a list of acceptable voltage/current levels for the PD controller
    #define PD_POWER_SETTINGS_MA                        {2500}      //The source PDOs must include the exact voltage, and have a current capability of at least what is given in our list here
    #define DEFAULT_SUPPLY_VOLTAGE_MV                   5000        //Default voltage prior to a contract being established
    #define DEFAULT_SUPPLY_MAX_CURRENT_MA               1000        //Default maximum current draw we are rated for, prior to a contract being established
    #define CELL_CURRENT_LIMIT_MARGIN_MA                100         //The soft current limit is set to the PD contract's negotiated current - this margin
    #define CELL_CURRENT_LOGGING_RESET_MARGIN_MA        200         //If the cell current limit is hit and logged it must fall below (PD contract max current - this margin) before it will be logged again
    #define MAX_OP_SYS_CURRENT_MARGIN_MA                50          //Hard current limit set to this much below PD contract's maximum, if hit all charging functionality will stop
    #define UVLO_THRESHOLD_OFF_MV                       9000        //If system voltage is below this thershold, the pad goes to the UVLO state and waits for it to rise
    #define UVLO_THRESHOLD_ON_MV                        10000       //The system voltage needs to be above this thershold for normal operation
#else
    #define SUPPLY_VOLTAGE_MV                           16000       //Nominal supply voltage, hard-set when no PD controller
    #define SUPPLY_MAX_CURRENT_MA                       6000        //Absolute maximum current draw we are rated for, hard-set when no PD controller
    #define CELL_CURRENT_LIMIT_MARGIN_MA                100         //The soft current limit is set to MAX_CURRENT - this margin
    #define CELL_CURRENT_LOGGING_RESET_MARGIN_MA        200         //If the cell current limit is hit and logged it must fall below (MAX_CURRENT - this margin) before it will be logged again
    #define MAX_OP_SYS_CURRENT_MARGIN_MA                50          //Hard current limit set to MAX_CURRENT - this margin, if hit all charging functionality will stop
    #define UVLO_THRESHOLD_OFF_MV                       10000       //If system voltage is below this thershold, the pad goes to the UVLO state and waits for it to rise
    #define UVLO_THRESHOLD_ON_MV                        11000       //The system voltage needs to be above this thershold for normal operation
#endif

#endif // SETTINGS_H
