/*******************************************************************************
           FILE:  STATE_MACHINE_Private.h

    DESCRIPTION:  State machine handler module

         AUTHOR:  Eric Goodchild
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  12/10/19
*******************************************************************************/
#ifdef STATE_MACHINE_PRIVATE_H
#error "STATE_MACHINE_Private.h was incorrectly included in " __FILE__
#else
#define STATE_MACHINE_PRIVATE_H
#endif

//Private defines, enums, typedefs, etc.

//#define STATE_MACHINE_UART_DEBUG    //if defined the state change debug data will be sent out the debug UART
