/*******************************************************************************
           FILE:  SECTIMER_Private.h

    DESCRIPTION:  Second timing API.

         AUTHOR:  John L. Winters
      COPYRIGHT:  Copyright 2019, Aira Inc.
        CREATED:  2/26/2020
*******************************************************************************/
#ifdef SECTIMER_PRIVATE_H
#error "SECTIMER_Private.h was incorrectly included in " __FILE__
#else
#define SECTIMER_PRIVATE_H
#endif

#include <stdint.h>
#include "SECTIMER_Public.h"

//Private defines, enums, typedefs, etc.

#define SECTIMER_MAX_PERIOD_SEC 0xFFFFFFFF  //the max possible timing interval

typedef struct 
{
    uint32_t counter_sec;   //Seconds counting up from zero after reset
    uint32_t trigger_ms;    //Next target increment second counter in ms units
} sectimer_context_t;

//Private Variables

static sectimer_context_t sectimer_context;
