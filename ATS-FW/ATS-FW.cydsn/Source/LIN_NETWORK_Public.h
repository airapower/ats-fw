/*******************************************************************************
           FILE:  LIN_NETWORK_Public.h

    DESCRIPTION:  LIN Network definition header

         AUTHOR:  Jules Laffitte
      COPYRIGHT:  Copyright 2021, Aira Inc.
        CREATED:  06/29/2021
*******************************************************************************/
#ifndef LIN_NETWORK_PUBLIC_H
#define LIN_NETWORK_PUBLIC_H

#include <stdint.h>

#define LIN_BREAK_BITS_COUNT    (13u)   // Number of bits in the break field

#define LIN_SYNC_FIELD_VALUE    ((uint8_t) 0x55u) // SYNC byte to ensure correct slave timings

#define LIN_HEADER_SIZE         (2u)    // Number of bytes in a LIN frame header
#define LIN_HEADER_SYNC_POS     (0u)    // Position of the SYNC byte in the LIN frame header
#define LIN_HEADER_PID_POS      (1u)    // Position of the PID byte in the LIN frame header
#define LIN_MAX_FRAME_SIZE      (8u)    // Maximum number of data bytes in a LIN frame

#define LIN_PDU_SF_DATA_SIZE    (5u)    // Maximum number of data bytes in a Start Frame (SF)
#define LIN_PDU_FF_DATA_SIZE    (4u)    // Maximum number of data bytes in a First Frame (FF)
#define LIN_PDU_CF_DATA_SIZE    (6u)    // Maximum number of data bytes in a Consecutive Frame (CF)

typedef enum
{
    LIN_SINGLE_FRAME_PCI        = 0x00, // Protocol control information type for SF
    LIN_FIRST_FRAME_PCI         = 0x10, // Protocol control information type for FF
    LIN_CONSECUTIVE_FRAME_PCI   = 0x20, // Protocol control information type for CF
    LIN_PCI_DATA_MASK           = 0x0F, // Protocol control information bits for data
    LIN_PCI_TYPE_MASK           = 0xF0, // Protocol control information bits for type
} lin_pci_t;

typedef struct __attribute__((packed))
{
    uint8_t node_address;           // Node address of slave participating in current PDU traffic
    uint8_t protocol_control_info;  // PCI byte for PDU frame

    union
    {
        struct __attribute__((packed))
        {
            uint8_t service_id;                     // SID/RSID byte for SF
            uint8_t data[LIN_PDU_SF_DATA_SIZE];     // data bytes for SF
        } single_frame;

        struct __attribute__((packed))
        {
            uint8_t length;                         // lower 8bits of the message length
            uint8_t service_id;                     // SID/RSID byte for FF
            uint8_t data[LIN_PDU_FF_DATA_SIZE];     // data bytes for FF
        } first_frame;

        struct __attribute__((packed))
        {
            uint8_t data[LIN_PDU_CF_DATA_SIZE];     // data bytes for CF
        } consecutive_frame;
    };
} lin_pdu_t;

typedef enum
{
    LIN_MASTER_REQUEST_FRAME_ID = 0x3C, // Master request frames are Frame ID 60
    LIN_SLAVE_RESPONSE_FRAME_ID = 0x3D, // Slave response frames are frame ID 61
    LIN_RESERVED_0_FRAME_ID     = 0x3E, // Frame ID 62 is reserved
    LIN_RESERVED_1_FRAME_ID     = 0x3F, // Frame ID 63 is reserved
} lin_reserved_frame_ids_t;

typedef enum
{
    LIN_SLOT_SUBSCRIBE,                 // The schedule slot is for a subscribe frame (slave -> master)
    LIN_SLOT_PUBLISH,                   // The schedule slot is for a publish frame (master -> slave)
} lin_slot_type_t;

typedef struct
{
    const uint8_t id;                   // Frame ID for the slot
    const lin_slot_type_t direction;    // Frame direction with respect to the master node
    const uint8_t length;               // Bytes in the frame assigned to this slot
    uint8_t data[LIN_MAX_FRAME_SIZE];   // Buffer for frame data
} lin_slot_t;

typedef struct
{
    const uint8_t offset_ms;        // Offset from end of previous LIN frame
    const uint8_t response_wait_ms; // Maximum subscribe frame slot duration
    lin_slot_t slot;                // Schedule slot parameters
} lin_schedule_entry_t;

extern lin_schedule_entry_t master_request_entry; // Predefined master request entry
extern lin_schedule_entry_t slave_response_entry; // Predefined slave response entry

/////////// Apache V1 network /////////

#define LIN_APACHE_NUM_ENTRIES  (4u)    // Number of entries in the Apache schedule

typedef enum
{
    LIN_APACHE_CELLS_DRIVE_FRAME_ID = 0x29,
    LIN_APACHE_CELLS_STATUS_FRAME_ID = 0x2A,
    LIN_APACHE_COMMAND_FRAME_ID = 0x2B,
    LIN_APACHE_CONTROL_FRAME_ID = 0x2C,
} lin_apache_frame_ids_t;

extern lin_schedule_entry_t apache_entries[LIN_APACHE_NUM_ENTRIES];

#endif // LIN_NETWORK_PUBLIC_H
